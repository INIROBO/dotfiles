#ifndef IO_H
#define IO_H

void special_keyboard(int key, int x, int y);
void keyboard(unsigned char key, int x, int y);
void print_help();

#endif // IO_H
