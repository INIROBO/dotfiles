#ifndef MATERIALS_H
#define MATERIALS_H

int read_file(char* filename, GLfloat* settings);

void set_material(object* aux_object);

#endif // MATERIALS_H
