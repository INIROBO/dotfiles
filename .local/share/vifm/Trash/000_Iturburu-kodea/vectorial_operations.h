#ifndef VECTORIAL_OPERATIONS_H_
#define VECTORIAL_OPERATIONS_H_

vector3 minus(point3 e1, point3 e0);

vector3 normal_sum(vector3 v0, vector3 v1);

vector3 cross(vector3 v0, vector3 v1);

GLdouble magnitude(vector3 v0);

vector3 division(vector3 v0, GLdouble m);

void showVector(vector3 v);

void showPoint(point3 p);

#endif // VECTORIAL_OPERATIONS_H_
