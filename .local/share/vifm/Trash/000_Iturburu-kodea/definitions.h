#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <GL/gl.h>
#include <math.h>

/** DEFINITIONS **/

/* Objektuen egoera */
#define KG_NORMAL_STATE                     0
#define KG_LOCKED_ON_LOCAL                  1
#define KG_LOCKED_ON_GLOBAL                 2
#define KG_LOCKED_X                         4
#define KG_LIGHT                            8
 
/** GLOBAL VARIBALES TO IDENTIFY TRANSFORMATIONS **/
#define KG_TRANSLATE                        0
#define KG_ROTATE                           1
#define KG_MODIFY                           2
#define KG_REFLECT                          3

/** TRANSFORMAZI MOTA DEFINITZEKO ERABILTZEN DIREN BALIOAK **/
#define KG_GLOBAL_TRANSFORMATION            0
#define KG_LOCAL_TRANSFORMATION             1

/** LEIHOAREN PROPIETATEAK **/
#define KG_WINDOW_TITLE                     "KbG 3 Entrega - Inigo Ortega (iortega045@ikasle.ehu.eus)"
#define KG_WINDOW_WIDTH                     800
#define KG_WINDOW_HEIGHT                    600
#define KG_WINDOW_X                         50
#define KG_WINDOW_Y                         50

/** ERABILTZAILEAREKN INTERAKUNTZA EGITEKO MEZUAK **/
#define KG_MSSG_SELECT_FILE                 "Idatz ezazu fitxategiaren path-a: "
#define KG_MSSG_FILENOTFOUND                "Fitxategi hori ez da existitzen!!"
#define KG_MSSG_INVALIDFILE                 "Arazoren bat egon da fitxategiarekin ..."
#define KG_MSSG_EMPTYFILE                   "Fitxategia hutsik dago"
#define KG_MSSG_FILEREAD                    "Fitxategiaren irakurketa buruta"
#define KG_MSSG_LIGHTING_OFF                "Argiztapena itzalita dago."
#define KG_MSSG_LIGHT_OFF                   "argia ez dago piztuta."

/** MUGIMENDUA ZENBATEAKOA DEFINITZEKO EREMUA **/
#define KG_STEP_MOVE                        5.0f
#define KG_STEP_ROTATE                      10.0f
#define KG_STEP_ZOOM                        0.75
#define KG_STEP_CAMERA_ANGLE                5.0f

/** HASIERAKO ERTZEN PERSPEKTIBA **/
#define KG_ORTHO_X_MIN_INIT                -5
#define KG_ORTHO_X_MAX_INIT                 5
#define KG_ORTHO_Y_MIN_INIT                -5
#define KG_ORTHO_Y_MAX_INIT                 5
#define KG_ORTHO_Z_MIN_INIT                -100
#define KG_ORTHO_Z_MAX_INIT                 10000

/** KOLOREAK **/
/* Atzeko planoaren koloreak RGBn */
#define KG_COL_BACK_R                       0.30f
#define KG_COL_BACK_G                       0.30f
#define KG_COL_BACK_B                       1.00f
#define KG_COL_BACK_A                       0.30f

/* Aukeratutako objektuaren kolorea */
#define KG_COL_SELECTED_R                   0.00f
#define KG_COL_SELECTED_G                   0.75f
#define KG_COL_SELECTED_B                   0.00f

/* Aukeratu gabeko objektuen kolorea */
#define KG_COL_NONSELECTED_R                1.00f
#define KG_COL_NONSELECTED_G                1.00f
#define KG_COL_NONSELECTED_B                1.00f

/* X Ardatzaren kolorea RGBn */
#define KG_COL_X_AXIS_R                     0.0f
#define KG_COL_X_AXIS_G                     1.0f
#define KG_COL_X_AXIS_B                     0.0f

/* Y Ardatzaren kolorea RGBn */
#define KG_COL_Y_AXIS_R                     1.0f
#define KG_COL_Y_AXIS_G                     0.0f
#define KG_COL_Y_AXIS_B                     0.0f

/* Z Ardatzaren kolorea RGBn */
#define KG_COL_Z_AXIS_R                     1.0f
#define KG_COL_Z_AXIS_G                     0.0f
#define KG_COL_Z_AXIS_B                     1.0f

/* Kuadrikularen kolorea RGBn */
#define KG_COL_GRID_R                       1.0f
#define KG_COL_GRID_G                       1.0f
#define KG_COL_GRID_B                       1.0f

/** KOMA HIGIKORREKO ZENBAKIRIK HANDIENA **/
#define KG_MAX_DOUBLE                       9999//10E25

/** KUADRIKULAREN LERROEN DISTANTZIA UNITARIOA **/
#define KG_DISTANCE_GRID_X                  1
#define KG_DISTANCE_GRID_Y                  1

/** TRANSFORMAZIO TASAK GLOBALEAN **/
/* Biraketa */
#define KG_X_ROTATION_VALUE                 0.04908738521234052 /* pi/64 */
#define KG_Y_ROTATION_VALUE                 0.04908738521234052 /* pi/64 */
#define KG_Z_ROTATION_VALUE                 0.04908738521234052 /* pi/64 */

/* Translazioa */
#define KG_X_TRANSLATION_VALUE              1
#define KG_Y_TRANSLATION_VALUE              1
#define KG_Z_TRANSLATION_VALUE              1

/* Tamaina aldaketa */
#define KG_X_MODIFICATION_VALUE             1
#define KG_Y_MODIFICATION_VALUE             1
#define KG_Z_MODIFICATION_VALUE             1

/** TRANSFORMAZIO TASAK LOKALEAN **/
/* Matrizeen tamaina */
#define KG_MATRIX_SIZE                      16

/* Biraketa */
#define KG_LOCAL_X_ROTATION_VALUE           0.04908738521234052 /* pi/64 */
#define KG_LOCAL_Y_ROTATION_VALUE           0.04908738521234052 /* pi/64 */
#define KG_LOCAL_Z_ROTATION_VALUE           0.04908738521234052 /* pi/64 */

/* Translazioa */
#define KG_LOCAL_X_TRANSLATION_VALUE        1
#define KG_LOCAL_Y_TRANSLATION_VALUE        1
#define KG_LOCAL_Z_TRANSLATION_VALUE        1

/* Tamaina aldaketa */
#define KG_LOCAL_X_MODIFICATION_VALUE       1
#define KG_LOCAL_Y_MODIFICATION_VALUE       1
#define KG_LOCAL_Z_MODIFICATION_VALUE       1

/** GLOBALEAN ETA LOKALEAN **/
/* Islapena */
#define KG_X_REFLECT_VALUE                  -1
#define KG_Y_REFLECT_VALUE                  -1
#define KG_Z_REFLECT_VALUE                  -1

/** KAMERAK **/
/* Motak */
#define KG_ORTHO_CAM                         0
#define KG_PERSPECTIVE_CAM                   1
#define KG_MOVING_CAM                        2

/* Ikuste eremua */
#define KG_CAM_ZNEAR                         10
#define KG_CAM_ZFAR                          500
#define KG_CAM_FOVY_DEFAULT                  35

/** ARGIAK **/
/* Motak */
#define KG_POINT_LIGHT                       0 
#define KG_DIRECTIONAL_LIGHT                 1 
#define KG_SPOT_LIGHT                        2 
#define KG_CAMERA_LIGHT                      3

/* Izenak */
#define KG_NAME_POINT_LIGHT                  "Bonbila"
#define KG_NAME_DIRECTIONAL_LIGHT            "Eguzkia"
#define KG_NAME_SPOT_LIGHT                   "Fokua"
#define KG_NAME_CAMERA_LIGHT                 "Kamara argia"

/* Koloreak */
#define KG_LIGHT_DIFFUSE0                    0.0
#define KG_LIGHT_DIFFUSE1                    1.0
#define KG_LIGHT_DIFFUSE2                    1.0
#define KG_LIGHT_DIFFUSE3                    1.0
#define KG_LIGHT_AMBIENT0                    0.2
#define KG_LIGHT_AMBIENT1                    0.2
#define KG_LIGHT_AMBIENT2                    0.2
#define KG_LIGHT_AMBIENT3                    1.0
#define KG_LIGHT_SPECULAR0                   1.0
#define KG_LIGHT_SPECULAR1                   1.0
#define KG_LIGHT_SPECULAR2                   1.0
#define KG_LIGHT_SPECULAR3                   1.0

/* Kopurua */
#define KG_MAX_LIGHTS                        5

/** MATERIALAK **/
#define KG_VALUE_MAX_LENGTH                  10
#define KG_SELECTED_OBJECT_MAT_INCR          1.4

/** PROGRAMAZIOA **/
#define KG_BUFFER_SIZE                       1024

/** STRUCTURES **/

/****************************
 * Structure to store the   *
 * coordinates of 3D points *
 ****************************/
typedef struct {
    GLdouble x, y, z;                       /* Puntu bat */
} point3;

/*****************************
 * Structure to store the    *
 * coordinates of 3D vectors *
 *****************************/
typedef struct {
    GLdouble x, y, z;                       /* Bektore bat */
} vector3;

/****************************
 * Structure to store the   *
 * colors in RGB mode       *
 ****************************/
typedef struct {
    GLfloat r, g, b;                        /* Koloreak RGBn */
} color3;

/****************************
 * Structure to store       *
 * objects' vertices         *
 ****************************/
typedef struct {
    point3 coord;                           /* coordinates,x, y, z */
    GLint num_faces;                        /* number of faces that share this vertex */
    vector3 normal;                         /* normal vector */
} vertex;

/****************************
 * Structure to store       *
 * objects' faces or        *
 * polygons                 *
 ****************************/
typedef struct {
    GLint num_vertices;                     /* number of vertices in the face */
    GLint *vertex_table;                    /* table with the index of each vertex */
    vector3 normal;                         /* normal vector */
} face;

/****************************
 * Structure to store       *
 * transformations history  *
 * (chronological order)    *
 ****************************/
struct transformation_history{
    GLdouble* matrix;                       /* Transformazio matrizea */
    struct transformation_history *next;    /* Hurrengo transformazio */
    struct transformation_history *previous;/* Aurreko transformazioa */
};
typedef struct transformation_history transformation_history;

/****************************
 * Structure to store a     *
 * 3D object                *
 ****************************/
struct object3d{
    char* file_name;                        /* filename for the object */
    GLint num_vertices;                     /* number of vertices in the object*/
    vertex *vertex_table;                   /* table of vertices */
    GLint num_faces;                        /* number of faces in the object */
    face *face_table;                       /* table of faces */
    GLint smooth;                           /* if it is smooth or not */
    point3 min;                             /* coordinates' lower bounds */
    point3 max;                             /* coordinates' bigger bounds */
    GLfloat* material;                      /* values to set up the material */
};

typedef struct object3d object3d;

/****************************
 * Structure to store a     *
 * pile of objects       *
 ****************************/
struct object{
    int state;
    struct object *next;                          /* next element in the pile of objects */
    object3d* obj3d;                              /* Corresponding 3D object */
    transformation_history* transformations_last; /* Azken transformazioa */
    GLint num_transformations;                    /* number of transformation matrices */
};

typedef struct object object;

/****************************
 * Structure to store a     *
 * light source properties  *
 ****************************/
struct light_properties{
    GLint type;                             /* Type of light */
    GLfloat cons_att;                       /* Constant attenuation */
    GLfloat lin_att;                        /* Linear attenuation */
    GLfloat qua_att;                        /* Quadratic attenuation */
    GLfloat spot_cutoff;                    /* Spot CutOff */
    GLfloat spot_exp;                       /* Spot exponent */
};
typedef struct light_properties light_properties;

#endif // DEFINITIONS_H
