/*********************************************
 * File with the function to load objects    *
 * from files                                *
 * ----------------------------------------- *
 * Author: Borja Calvo (borja.calvo@ehu.es)  *
 * Date:   September 2014                    *
 *********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "definitions.h"
#include "vectorial_operations.h"

#define MAXLINE 200

extern object * _object_to_transform;

/*
 * Auxiliar function to process each line of the file
 */
static int sreadint(char * lerroa, int * zenbakiak) {
    char *s = lerroa;
    int i, zbk, kont = 0;

    while (sscanf(s, " %d%n", &zbk, &i) > 0) {
        s += i;
        zenbakiak[kont++] = zbk;
    }
    return (kont);
}

static int sreadint2(char * lerroa, int * zenbakiak) {
    char *s = lerroa;
    int i, zbk, kont = 0;

    while (sscanf(s, " %d%n", &zbk, &i) > 0) {
        s += i;
    while ((*s != ' ')&&(*s !='\0')) s++;  // jump vector normal information
        zenbakiak[kont++] = zbk;
    }
printf("%d numbers in the line\n",kont);
    return (kont);
}

/**
 * @brief Function to read wavefront files (*.obj)
 * @param file_name Path of the file to be read
 * @param object_ptr Pointer of the object3d type structure where the data will be stored
 * @return Result of the reading: 0=Read ok, 1=File not found, 2=Invalid file, 3=Empty file
 */
int read_wavefront(char * file_name, object * object_ptr) {
    vertex *vertex_table;
    face *face_table;
    int num_vertices = -1, num_faces = -1, count_vertices = 0, count_faces = 0;
    FILE *obj_file;
    char line[MAXLINE], line_1[MAXLINE], aux[45];
    int k;
    int i, j;
    int v, f, v_index;
    int values[MAXLINE];
    object_ptr->obj3d = (object3d*) malloc (sizeof(object3d));
    object3d* aux_object = object_ptr->obj3d;


    /*
     * The function reads twice the file. In the first read the number of
     * vertices and faces is obtained. Then, memory is allocated for each
     * of them and in the second read the actual information is read and
     * loaded. Finally, the object structure is created
     */
    if ((obj_file = fopen(file_name, "r")) == NULL) return (1);
    while (fscanf(obj_file, "\n%[^\n]", line) > 0) {
        i = 0;
        while (line[i] == ' ') i++;
        if ((line[0] == '#') && ((int) strlen(line) > 5)) {
            i += 2;
            j = 0;
            while (line[i] != ' ') line_1[j++] = line[i++];
            i++;
            line_1[j] = '\0';
            j = 0;
            while ((line[i] != ' ') && (line[i] != '\0'))
                aux[j++] = line[i++];
            aux[j] = 0;
            if (strcmp(aux, "vertices") == 0) {
                num_vertices = atoi(line_1);
            }
            if (strncmp(aux, "elements", 7) == 0) {
                num_faces = atoi(line_1);
            }
        } else {
            if (strlen(line) > 6) {
                if (line[i] == 'f' && line[i + 1] == ' ')
                    count_faces++;
                else if (line[i] == 'v' && line[i + 1] == ' ')
                    count_vertices++;
            }
        }
    }
    fclose(obj_file);
    printf("1 pasada: num vert = %d (%d), num faces = %d(%d) \n",num_vertices,count_vertices,num_faces,count_faces);
    if ((num_vertices != -1 && num_vertices != count_vertices) || (num_faces != -1 && num_faces != count_faces)) {
        printf("WARNING: full file format: (%s)\n", file_name);
        //return (2);
    }
    if (num_vertices == 0 || count_vertices == 0) {
        printf("No vertex found: (%s)\n", file_name);
        return (3);
    }
    if (num_faces == 0 || count_faces == 0) {
        printf("No faces found: (%s)\n", file_name);
        return (3);
    }

    num_vertices = count_vertices;
    num_faces = count_faces;

    vertex_table = (vertex *) malloc(num_vertices * sizeof (vertex));
    face_table = (face *) malloc(num_faces * sizeof (face));

    obj_file = fopen(file_name, "r");
    k = 0;
    j = 0;

    for (i = 0; i < num_vertices; i++)
        vertex_table[i].num_faces = 0;

    while (fscanf(obj_file, "\n%[^\n]", line) > 0) {
        switch (line[0]) {
            case 'v':
            if (line[1] == ' ')  // vn not interested
        {
                sscanf(line + 2, "%lf%lf%lf", &(vertex_table[k].coord.x),
                        &(vertex_table[k].coord.y), &(vertex_table[k].coord.z));
                k++;
        }
               break;

            case 'f':
        if (line[1] == ' ') // fn not interested
                {
                for (i = 2; i <= (int) strlen(line); i++)
                    line_1[i - 2] = line[i];
        line_1[i-2] = '\0';
                face_table[j].num_vertices = sreadint2(line_1, values);
    //printf("f %d vertices\n",face_table[j].num_vertices);
                face_table[j].vertex_table = (int *) malloc(face_table[j].num_vertices * sizeof (int));
                for (i = 0; i < face_table[j].num_vertices; i++) {
                    face_table[j].vertex_table[i] = values[i] - 1;
    //printf(" %d ",values[i] - 1);
                    vertex_table[face_table[j].vertex_table[i]].num_faces++;
                    }
    //printf("\n");
                j++;
                }
              break;
        }
    }

    /* Aurpegi bakoitzaren  normalak kalkulatu */
    for (i = 0;  i < num_faces ; i++)
    {
        /* Aurpegi batean hiru puntu jaso */
        point3 e0 = vertex_table[face_table[i].vertex_table[0]].coord;
        point3 e1 = vertex_table[face_table[i].vertex_table[1]].coord;
        point3 e2 = vertex_table[face_table[i].vertex_table[2]].coord;
        
        /* Plano berdinean dauden bi bektore lortu */
        vector3 a = minus(e1, e0);
        vector3 b = minus(e2, e0);

        /* x biderkadura egin */
        vector3 multiplication = cross(a, b);

        /* Modulua lortu */
        GLdouble mag = magnitude(multiplication);

        /* bektorea eta moduluaren arteko zatiketa egin,
         * bektore unitario bat lortzeko */
        vector3 n = division(multiplication, mag);

        face_table[i].normal = n;

    }

    /* Erpin bakoitzaren normalen zerrenda hasieratu */
    vector3 normal_table[num_vertices];
    for (v = 0; v < num_vertices; v++)
    {
        normal_table[v].x = 0;
        normal_table[v].y = 0;
        normal_table[v].z = 0;
    }

    /* Erpin bakoitzaren aurpegien normalen baturak jaso */
    for (f = 0; f < num_faces; f++) {
        for (v = 0; v < face_table[f].num_vertices; v++) {
            v_index = face_table[f].vertex_table[v];
            normal_table[v_index] = normal_sum(normal_table[v_index], face_table[f].normal);
        }
    }

    /* Zatidura aplikatu batezbestekoa izateko */
    for (v = 0; v < num_vertices; v++)
    {
        normal_table[v].x /= vertex_table[v].num_faces;
        normal_table[v].y /= vertex_table[v].num_faces;
        normal_table[v].z /= vertex_table[v].num_faces;

        vertex_table[v].normal = normal_table[v];
    }


    fclose(obj_file);

    printf("3 pasada\n");

    /*
     * Information read is introduced in the structure */
    object_ptr->num_transformations = 0;
    aux_object->vertex_table = vertex_table;
    aux_object->face_table = face_table;
    aux_object->num_vertices = num_vertices;
    aux_object->num_faces = num_faces;
    aux_object->smooth = 0;
    aux_object->material = 0;
    aux_object->file_name = file_name;
    object_ptr->state = KG_NORMAL_STATE;

    /*
     * The maximum and minimum coordinates are obtained **/
    aux_object->max.x = aux_object->vertex_table[0].coord.x;
    aux_object->max.y = aux_object->vertex_table[0].coord.y;
    aux_object->max.z = aux_object->vertex_table[0].coord.z;
    aux_object->min.x = aux_object->vertex_table[0].coord.x;
    aux_object->min.y = aux_object->vertex_table[0].coord.y;
    aux_object->min.z = aux_object->vertex_table[0].coord.z;

    for (i = 1; i < aux_object->num_vertices; i++)
    {
        if (aux_object->vertex_table[i].coord.x < aux_object->min.x)
            aux_object->min.x = aux_object->vertex_table[i].coord.x;

        if (aux_object->vertex_table[i].coord.y < aux_object->min.y)
            aux_object->min.y = aux_object->vertex_table[i].coord.y;

        if (aux_object->vertex_table[i].coord.z < aux_object->min.z)
            aux_object->min.z = aux_object->vertex_table[i].coord.z;

        if (aux_object->vertex_table[i].coord.x > aux_object->max.x)
            aux_object->max.x = aux_object->vertex_table[i].coord.x;

        if (aux_object->vertex_table[i].coord.y > aux_object->max.y)
            aux_object->max.y = aux_object->vertex_table[i].coord.y;

        if (aux_object->vertex_table[i].coord.z > aux_object->max.z)
            aux_object->max.z = aux_object->vertex_table[i].coord.z;

    }

    /* Kargatutako objektua izango da transformazioetarako aukeratua. */
    _object_to_transform = object_ptr;
    return (0);
}

