#include "definitions.h"
#include "load_obj.h"
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <libgen.h>
#include <math.h>
#include "matrizeak.h"

extern int _transformation_objective;

/**
 * @brief X ardatzarekiko mugitu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* translationX(float direction)
{
    float constant;
    if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
    {
        constant = KG_X_TRANSLATION_VALUE;
    }
    else
    {
        constant = KG_LOCAL_X_TRANSLATION_VALUE;
    }

    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=constant*direction;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}


/**
 * @brief Y ardatzarekiko mugitu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* translationY(float direction)
{
    float constant;
    if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
    {
        constant = KG_Y_TRANSLATION_VALUE;
    }
    else
    {
        constant = KG_LOCAL_Y_TRANSLATION_VALUE;
    }

    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=KG_Y_TRANSLATION_VALUE*direction;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Z ardatzarekiko mugitu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* translationZ(float direction)
{
    float constant;
    if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
    {
        constant = KG_Z_TRANSLATION_VALUE;
    }
    else
    {
        constant = KG_LOCAL_Z_TRANSLATION_VALUE;
    }

    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=KG_Z_TRANSLATION_VALUE*direction;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Y ardatzarekiko biratu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* rotationY(float direction)
{
    float theta;
    if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
    {
        theta = KG_X_ROTATION_VALUE*direction;
    }
    else
    {
        theta = KG_LOCAL_X_ROTATION_VALUE*direction;
    }

    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=cos(theta);   m[9] =-sin(theta);    m[13]=0;
    m[2]=0;   m[6]=sin(theta);   m[10]=cos(theta);    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief X ardatzarekiko biratu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* rotationX(float direction)
{
    float theta;
    if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
    {
        theta = KG_Y_ROTATION_VALUE*direction;
    }
    else
    {
        theta = KG_LOCAL_Y_ROTATION_VALUE*direction;
    }

    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=cos(theta);   m[4]=0;   m[8] =-sin(theta);    m[12]=0;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=0;
    m[2]=sin(theta);   m[6]=0;   m[10]=cos(theta);    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Z ardatzarekiko biratu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* rotationZ(float direction)
{
    float theta;
    if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
    {
        theta = KG_Z_ROTATION_VALUE*direction;
    }
    else
    {
        theta = KG_LOCAL_Z_ROTATION_VALUE*direction;
    }
    float constant;

    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=cos(theta);   m[4]=sin(theta);   m[8] =0;    m[12]=0;
    m[1]=-sin(theta);   m[5]=cos(theta);   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief X ardatzarekiko tamaina aldatu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* modificationX(float direction)
{
    float constant;
    if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
    {
        constant = KG_X_MODIFICATION_VALUE;
    }
    else
    {
        constant = KG_LOCAL_X_MODIFICATION_VALUE;
    }

    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=constant*direction;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Y ardatzarekiko tamaina aldatu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* modificationY(float direction)
{
    float constant;
    if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
    {
        constant = KG_Y_MODIFICATION_VALUE;
    }
    else
    {
        constant = KG_LOCAL_Y_MODIFICATION_VALUE;
    }

    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=constant*direction;   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Z ardatzarekiko tamaina aldatu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* modificationZ(float direction)
{
    float constant;
    if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
    {
        constant = KG_Z_MODIFICATION_VALUE;
    }
    else
    {
        constant = KG_LOCAL_Z_MODIFICATION_VALUE;
    }

    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=constant*direction;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief X ardatzarekiko isladatu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* reflectX(float direction)
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=KG_X_REFLECT_VALUE*direction;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Y ardatzarekiko isladatu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* reflectY(float direction)
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=KG_Y_REFLECT_VALUE*direction;   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Z ardatzarekiko isladatu
 * @param direction Noranzkoa
 * @return Transformazio matrizea
 */
GLdouble* reflectZ(float direction)
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=KG_Z_REFLECT_VALUE*direction;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Returns the initial matrix for perspective cam
 * @return Initial matrix for perspective cam
 */
GLdouble* persCamInitialMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=0.903989;   m[4]=-0.144039;   m[8] =0.402562;    m[12]=9.708802;
    m[1]=0;          m[5]=0.941544;    m[9] =0.336890;    m[13]=7.684449;
    m[2]=-0.427555;  m[6]=-0.304545;   m[10]=0.851146;    m[14]=20.527536;
    m[3]=0;          m[7]=0;           m[11]=0;           m[15]=1;

    return m;
}

/**
 * @brief Returns the initial ecu matrix for perspective cam
 * @return Initial ecu matrix for perspective cam
 */
GLdouble* persCamInitialECUMatrix()
{
    GLdouble* ecu;
    ecu = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    ecu[0]=0;   ecu[4]=0;   ecu[8] =0;    ecu[12]=0;
    ecu[1]=0;   ecu[5]=0;   ecu[9] =1;    ecu[13]=0;
    ecu[2]=0;   ecu[6]=-1;  ecu[10]=0;    ecu[14]=0;
    ecu[3]=1;   ecu[7]=1;   ecu[11]=0;    ecu[15]=0;

    return ecu;
}

/**
 * @brief Returns the initial matrix for moving cam
 * @return Initial matrix for moving cam
 */
GLdouble* movingCamInitialMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=2;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=23;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;
    /* m[0]=0.999201;   m[4]=-0.013946;   m[8] =-0.037458;    m[12]=0; */
    /* m[1]=0.014946;          m[5]=0.999536;    m[9] =0.026552;    m[13]=1.412878; */
    /* m[2]=0.037071;  m[6]=-0.027091;   m[10]=0.998946;    m[14]=20.349241; */
    /* m[3]=0;          m[7]=0;           m[11]=0;           m[15]=1; */

    return m;
}

/**
 * @brief Returns the initial ecu matrix for moving cam
 * @return Initial ecu matrix for moving cam
 */
GLdouble* movingCamInitialECUMatrix()
{
    GLdouble* ecu;
    ecu = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    ecu[0]=0;   ecu[4]=0;   ecu[8] =0;    ecu[12]=0;
    ecu[1]=0;   ecu[5]=0;   ecu[9] =1;    ecu[13]=0;
    ecu[2]=0;   ecu[6]=-1;  ecu[10]=0;    ecu[14]=0;
    ecu[3]=1;   ecu[7]=1;   ecu[11]=0;    ecu[15]=0;

    return ecu;
}

/**
 * @brief Returns the initial positioning matrix for directional lights.
 * @return Initial positioning matrix for directional lights.
 */
GLdouble* directionalLightInitialMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=0;   m[4]=0;   m[8] =1;    m[12]=0;
    m[1]=0;   m[5]=0;   m[9] =1;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Returns the initial positioning matrix for direct second light.
 * @return Initial positioning matrix for direct second light.
 */
GLdouble* directionalLightPositioningInitialMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=0;   m[4]=0;   m[8] =1;    m[12]=0;
    m[1]=0;   m[5]=0;   m[9] =1;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Returns the initial matrix for spot lights.
 * @return Initial matrix for spot lights.
 */
GLdouble* spotLightInitialMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=0;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=0;   m[9] =-1;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=0;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Returns the initial positioning matrix for third light.
 * @return Initial matrix for third light.
 */
GLdouble* spotLightPositioningInitialMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=10;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=10;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Returns the initial matrix for point light.
 * @return Initial matrix for point light.
 */
GLdouble* pointLightInitialMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=0;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=0;   m[9] =-1;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=0;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Returns the initial matrix for fourth light.
 * @return Initial matrix for fourth light.
 */
GLdouble* pointLight1PositioningInitialMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=5;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=5;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Returns the initial matrix for fifth light.
 * @return Initial matrix for fifth light.
 */
GLdouble* pointLight2PositioningInitialMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=-5;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=5;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

/**
 * @brief Matrize bat emanda, irteera standarrean bistaratzen du.
 * @param m M
 * @return Transformazio matrizea
 */
void showMatrix(GLdouble* m)
{
    printf("%f %f %f %f\n", m[0], m[4], m[8], m[12]);
    printf("%f %f %f %f\n", m[1], m[5], m[9], m[13]);
    printf("%f %f %f %f\n", m[2], m[6], m[10], m[14]);
    printf("%f %f %f %f\n", m[3], m[7], m[11], m[15]);
}

/**
 * @brief Bi matrizeren biderkadura itzultzen du.
 * @param matA Lehen matrizea
 * @param matA Bigarren matrizea
 * @return Emaitza matrizea
 */
GLdouble* multiplyM(GLdouble* matA, GLdouble* matB) {
    GLdouble* matC = (GLdouble*) calloc (KG_MATRIX_SIZE, sizeof(GLdouble ));

    int i, j, k;
    int dx = 0;
    int M = 4;
    int K = 4;
    int N = 4;
    for(i=0;i<M;i++){
        for(j=0;j<K;j++){
            for(k=0;k<N;k++){
                matC[dx]+=matA[i*M+k]*matB[k*N+j];
            }
            dx++;
        }
    }

    return matC;
}

GLdouble* copyMatrix(GLdouble* matrix)
{
    GLdouble* mat = (GLdouble*) calloc (KG_MATRIX_SIZE, sizeof(GLdouble ));
    int i;
    for (i = 0; i < KG_MATRIX_SIZE; i++)
        mat[i] = matrix[i];

    return mat;
}

GLdouble* identityMatrix()
{
    GLdouble* m;
    m = (GLdouble*) malloc (sizeof(GLdouble )*KG_MATRIX_SIZE);
    m[0]=1;   m[4]=0;   m[8] =0;    m[12]=0;
    m[1]=0;   m[5]=1;   m[9] =0;    m[13]=0;
    m[2]=0;   m[6]=0;   m[10]=1;    m[14]=0;
    m[3]=0;   m[7]=0;   m[11]=0;    m[15]=1;

    return m;
}

