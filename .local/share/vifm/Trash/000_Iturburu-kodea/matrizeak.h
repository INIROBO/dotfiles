#ifndef MATRIZEAK_H_
#define MATRIZEAK_H_

GLdouble* translationX(float direction);

GLdouble* translationY(float direction);

GLdouble* translationZ(float direction);

GLdouble* rotationX(float direction);

GLdouble* rotationY(float direction);

GLdouble* rotationZ(float direction);

GLdouble* modificationX(float direction);

GLdouble* modificationY(float direction);

GLdouble* modificationZ(float direction);

GLdouble* reflectX(float direction);

GLdouble* reflectY(float direction);

GLdouble* reflectZ(float direction);

GLdouble* persCamInitialMatrix();

GLdouble* persCamInitialECUMatrix();

GLdouble* movingCamInitialMatrix();

GLdouble* movingCamInitialECUMatrix();

GLdouble* directionalLightInitialMatrix();

GLdouble* directionalLightPositioningInitialMatrix();

GLdouble* spotLightInitialMatrix();

GLdouble* spotLightPositioningInitialMatrix();

GLdouble* pointLightInitialMatrix();

GLdouble* pointLight1PositioningInitialMatrix();

GLdouble* pointLight2PositioningInitialMatrix();

void showMatrix(GLdouble* m);

GLdouble* multiplyM(GLdouble* matA, GLdouble* matB);

GLdouble* copyMatrix(GLdouble* matrix);

GLdouble* identityMatrix();

#endif // MATRIZEAK_H_
