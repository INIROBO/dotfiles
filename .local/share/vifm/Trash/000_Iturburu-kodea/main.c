#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "display.h"
#include "io.h"
#include "definitions.h"
#include "matrizeak.h"
#include "lights.h"

/** GLOBAL VARIABLES **/

GLdouble _window_ratio;                     /*Control of window's proportions */
GLdouble _ortho_x_min,_ortho_x_max;         /*Variables for the control of the orthographic projection*/
GLdouble _ortho_y_min ,_ortho_y_max;        /*Variables for the control of the orthographic projection*/
GLdouble _ortho_z_min,_ortho_z_max;         /*Variables for the control of the orthographic projection*/

object * _first_object = 0;               /*List of objects*/
object * _selected_object = 0;            /*Object currently selected*/

GLint _camera_type = 0;

object * _perspective_camera = 0;         /*Perpective camara containing object*/
object * _moving_camera = 0;              /*Moving camara containing object*/

GLdouble* _moving_camera_orient_matrix;   /* The orientation of moving cam */

GLdouble _perspective_camera_fovy;        /* Perspective camera fovy value */
GLdouble _moving_camera_fovy;             /* Moving camera fovy value */

object * _object_to_transform = 0;        /* The object that will be transformed */

int _transformation;                      /* Transformation type */
int _transformation_objective;            /* Local or Global transformation */

/* Erabiltzaileak definitutako argiaren ezarpenak */
GLint _lights_on = 1;

/* Argien objektuak */
object* _light[KG_MAX_LIGHTS];            /* The object of each light */
GLint _selected_light = -1;               /* Which light is selected */
light_properties* _light_properties       /* The propeties of each individual light */
[KG_MAX_LIGHTS];

/**
 * @brief Argi iturri bat hasieratu
 * @param i argi zenbakia
 * @param Argi mota
 */
void initLight(int i, GLint type)
{
    switch (type)
    {
        case KG_CAMERA_LIGHT:
            _light_properties[i]->type = KG_CAMERA_LIGHT;
            _light_properties[i]->cons_att = 0.0;
            _light_properties[i]->lin_att = 0.0;
            _light_properties[i]->qua_att = 0.0;
            _light_properties[i]->spot_cutoff = 0.0;
            _light_properties[i]->spot_exp = 0.0;

            /* Argiaren objektua */
            _light[i]->state = KG_LIGHT;
            _light[i]->next = 0;
            _light[i]->obj3d = 0; /* Ez dute gorputz fisikorik */
            _light[i]->num_transformations = 0;
            _light[i]->transformations_last = 0;
            break;

        case KG_DIRECTIONAL_LIGHT:
            /* Argiak izango dituen ezaugarriak */
            _light_properties[i]->type = KG_DIRECTIONAL_LIGHT;
            _light_properties[i]->cons_att = 1.0;
            _light_properties[i]->lin_att = 0.0;
            _light_properties[i]->qua_att = 0.001;
            _light_properties[i]->spot_cutoff = 20.0;
            _light_properties[i]->spot_exp = 1.0;

            /* Argiaren objektua */
            _light[i]->state = KG_LIGHT;
            _light[i]->next = 0;
            _light[i]->obj3d = 0; /* Ez dute gorputz fisikorik */
            _light[i]->num_transformations = 0;
            _light[i]->transformations_last = (transformation_history *) malloc(sizeof (transformation_history));
            /* Lehen matrizea */
            _light[i]->transformations_last->matrix = directionalLightPositioningInitialMatrix();
            /* _light[i]->transformations_first = _light[i]->transformations_last; */
            break;

        case KG_SPOT_LIGHT:
            /* Argiak izango dituen ezaugarriak */
            _light_properties[i]->type = KG_SPOT_LIGHT;
            _light_properties[i]->cons_att = 1.0;
            _light_properties[i]->lin_att = 0.0;
            _light_properties[i]->qua_att = 0.001;
            _light_properties[i]->spot_cutoff = 20.0;
            _light_properties[i]->spot_exp = 1.0;

            /* Argiaren objektua */
            _light[i]->state = KG_LIGHT;
            _light[i]->next = 0;
            _light[i]->obj3d = 0; /* Ez dute gorputz fisikorik */
            _light[i]->num_transformations = 0;
            _light[i]->transformations_last = (transformation_history *) malloc(sizeof (transformation_history));
            /* Lehen matrizea */
            _light[i]->transformations_last->matrix = spotLightPositioningInitialMatrix();
            /* _light[i]->transformations_first = _light[i]->transformations_last; */
            break;

        case KG_POINT_LIGHT:
            if (i == 3)
            {
                /* Argiak izango dituen ezaugarriak */
                _light_properties[i]->type = KG_POINT_LIGHT;
                _light_properties[i]->cons_att = 1.0;
                _light_properties[i]->lin_att = 0.0;
                _light_properties[i]->qua_att = 0.01;
                _light_properties[i]->spot_cutoff = 20.0;
                _light_properties[i]->spot_exp = 1.0;

                /* Argiaren objektua */
                _light[i]->state = KG_LIGHT;
                _light[i]->next = 0;
                _light[i]->obj3d = 0 /* Ez dute gorputz fisikorik */;
                _light[i]->num_transformations = 0;
                _light[i]->transformations_last = (transformation_history *) malloc(sizeof (transformation_history));
                /* Lehen matrizea */
                _light[i]->transformations_last->matrix = pointLight1PositioningInitialMatrix();
                /* _light[i]->transformations_first = _light[i]->transformations_last; */
            }
            else
            {
                /* Argiak izango dituen ezaugarriak */
                _light_properties[i]->type = KG_POINT_LIGHT;
                _light_properties[i]->cons_att = 1.0;
                _light_properties[i]->lin_att = 0.0;
                _light_properties[i]->qua_att = 0.01;
                _light_properties[i]->spot_cutoff = 20.0;
                _light_properties[i]->spot_exp = 1.0;

                /* Argiaren objektua */
                _light[i]->state = KG_LIGHT;
                _light[i]->next = 0;
                _light[i]->obj3d = 0; /* Ez dute gorputz fisikorik */
                _light[i]->num_transformations = 0;
                _light[i]->transformations_last = (transformation_history *) malloc(sizeof (transformation_history));
                /* Lehen matrizea */
                _light[i]->transformations_last->matrix = pointLight2PositioningInitialMatrix();
                /* _light[i]->transformations_first = _light[i]->transformations_last; */
            }
            break;
    }
}

/**
 * @brief Argiak hasieratzen ditu.
 */
void lightsInitialization()
{
    /* Lehena izan ezik, argi guztiak piztuko dira */
    enableAllLights();
    enableLighting();

    /* Argien objektuak hasieratu */
    _light[0] = (object*) malloc (sizeof(object));
    _light[1] = (object*) malloc (sizeof(object));
    _light[2] = (object*) malloc (sizeof(object));
    _light[3] = (object*) malloc (sizeof(object));
    _light[4] = (object*) malloc (sizeof(object));

    /* Argien ezaugarriak gordeko dituen zerrenda hasieratu */
    _light_properties[0] = (light_properties*) malloc (sizeof(light_properties));
    _light_properties[1] = (light_properties*) malloc (sizeof(light_properties));
    _light_properties[2] = (light_properties*) malloc (sizeof(light_properties));
    _light_properties[3] = (light_properties*) malloc (sizeof(light_properties));
    _light_properties[4] = (light_properties*) malloc (sizeof(light_properties));

    /* Hasieratu argiak. */
    initLight(0, KG_CAMERA_LIGHT);
    initLight(1, KG_DIRECTIONAL_LIGHT);
    initLight(2, KG_SPOT_LIGHT);
    initLight(3, KG_POINT_LIGHT);
    initLight(4, KG_POINT_LIGHT);


}

/**
 * @brief Kamarak hasieratzen ditu.
 */
void camInitialization()
{
    /* Objektua */
    _perspective_camera = (object*) malloc (sizeof(object));
    _moving_camera = (object*) malloc (sizeof(object));

    /* Transformazioak */
    _perspective_camera->transformations_last = (transformation_history *) malloc(sizeof (transformation_history));
    _moving_camera->transformations_last = (transformation_history *) malloc(sizeof (transformation_history));

    /* Hasierako matrizeak */
    _perspective_camera->transformations_last->matrix = persCamInitialMatrix();
    _moving_camera->transformations_last->matrix = movingCamInitialMatrix();

    /* Lehen transformaziora lortu */
    /* _perspective_camera->transformations_first = _perspective_camera->transformations_last; */
    /* _moving_camera->transformations_first = _moving_camera->transformations_last; */

    /* Ez dute gorputz fisikorik */
    _perspective_camera->obj3d = 0;
    _moving_camera->obj3d = 0;

    /* Transformazio kopurua */
    _perspective_camera->num_transformations = 0;
    _moving_camera->num_transformations = 0;

    /* Egoerak */
    _perspective_camera->state = KG_NORMAL_STATE;
    _moving_camera->state = KG_LOCKED_ON_LOCAL + KG_LOCKED_X;
}

/**
 * @brief Hasierapen orokorra.
 */
void initialization (){


    /*Initialization of all the variables with the default values*/
    _ortho_x_min = KG_ORTHO_X_MIN_INIT;
    _ortho_x_max = KG_ORTHO_X_MAX_INIT;
    _ortho_y_min = KG_ORTHO_Y_MIN_INIT;
    _ortho_y_max = KG_ORTHO_Y_MAX_INIT;
    _ortho_z_min = KG_ORTHO_Z_MIN_INIT;
    _ortho_z_max = KG_ORTHO_Z_MAX_INIT;

    _window_ratio = (GLdouble) KG_WINDOW_WIDTH / (GLdouble) KG_WINDOW_HEIGHT;

    /*Definition of the background color*/
    glClearColor(KG_COL_BACK_R, KG_COL_BACK_G, KG_COL_BACK_B, KG_COL_BACK_A);

    /*Definition of the method to draw the objects*/
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    /* Kamarak hasieratu */
    camInitialization();

    /* Argiak hasieratu */
    lightsInitialization();

    /* Materialak gaitu */
    glEnable(GL_COLOR_MATERIAL);
}


/** MAIN FUNCTION **/
int main(int argc, char** argv) {

    _perspective_camera_fovy = KG_CAM_FOVY_DEFAULT;
    _moving_camera_fovy = KG_CAM_FOVY_DEFAULT;

    /* Defektuz nolakoak dira transformazioak: */
    _transformation = KG_TRANSLATE;
    _transformation_objective = KG_GLOBAL_TRANSFORMATION;

    /* Kamara ibilitariak duen orientazioa */
    _moving_camera_orient_matrix = identityMatrix();

    /*First of all, print the help information*/
    print_help();

    /* glut initializations */
    glutInit(&argc, argv);
    glutInitWindowSize(KG_WINDOW_WIDTH, KG_WINDOW_HEIGHT);
    glutInitWindowPosition(KG_WINDOW_X, KG_WINDOW_Y);
    glutCreateWindow(KG_WINDOW_TITLE);

    /* set the callback functions */
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special_keyboard);

    /* this initialization has to be AFTER the creation of the window */
    initialization();
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);


    /* start the main loop */
    glutMainLoop();
    return 0;
}
