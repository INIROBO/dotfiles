#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include "definitions.h"
#include "matrizeak.h"

extern GLint _lights_on;

extern object* _light[KG_MAX_LIGHTS];
extern GLint _selected_light;
extern light_properties* _light_properties[KG_MAX_LIGHTS];

/**
 * @brief Argia ezartzen du
 * @param i ezarri beharreko argia
 */
void setLight(int i)
{
    GLdouble* m;
    GLfloat norabidea[3];
    GLfloat kokapena[4];
    GLint aim;

    /* Argiaren koloreak */
    GLfloat diffuse_color [4] = { KG_LIGHT_DIFFUSE0, KG_LIGHT_DIFFUSE1, KG_LIGHT_DIFFUSE2, KG_LIGHT_DIFFUSE3 };
    GLfloat ambient_color [4] = { KG_LIGHT_AMBIENT0, KG_LIGHT_AMBIENT1, KG_LIGHT_AMBIENT2, KG_LIGHT_AMBIENT3 };
    GLfloat specular_color[4] = { KG_LIGHT_SPECULAR2, KG_LIGHT_SPECULAR2, KG_LIGHT_SPECULAR2, KG_LIGHT_SPECULAR3 };

    /* Erabiliko den argia aukeratu */
    switch (i)
    {
        case 0:
            aim = GL_LIGHT0;
            break;
        case 1:
            aim = GL_LIGHT1;
            break;
        case 2:
            aim = GL_LIGHT2;
            break;
        case 3:
            aim = GL_LIGHT3;
            break;
        case 4:
            aim = GL_LIGHT4;
            break;
    }

    /* Koloreak */
    glLightfv (aim , GL_AMBIENT , ambient_color);
    glLightfv (aim , GL_DIFFUSE , diffuse_color);
    glLightfv (aim , GL_SPECULAR , specular_color);

    /* Urruntzean argia desagertzeko */
    glLightf (aim , GL_CONSTANT_ATTENUATION , _light_properties[i]->cons_att);
    glLightf (aim , GL_LINEAR_ATTENUATION , _light_properties[i]->lin_att);
    glLightf (aim , GL_QUADRATIC_ATTENUATION , _light_properties[i]->qua_att);

    /* Zer motako argia den kontuan izateko */
    switch (_light_properties[i]->type)
    {
        case KG_POINT_LIGHT:

            m = multiplyM(pointLightInitialMatrix(), _light[i]->transformations_last->matrix);
            /* Matrizetik kokapena jaso */
            kokapena[0] = m[12];
            kokapena[1] = m[13];
            kokapena[2] = m[14];
            kokapena[3] = m[15];

            glLightfv (aim , GL_POSITION , kokapena);

            /* Bonbila motako argiak behar duen ezaugarria */
            glLightf (aim , GL_SPOT_CUTOFF , 180.0);
            break;

        case KG_DIRECTIONAL_LIGHT:

            m = multiplyM(directionalLightInitialMatrix(), _light[i]->transformations_last->matrix);
            kokapena[0] = m[8];
            kokapena[1] = m[9];
            kokapena[2] = m[10];
            kokapena[3] = m[11];

            glLightfv (aim , GL_POSITION , kokapena);
            break;

        case KG_SPOT_LIGHT:

            m = multiplyM(spotLightInitialMatrix(), _light[i]->transformations_last->matrix);

            kokapena[0] = m[12];
            kokapena[1] = m[13];
            kokapena[2] = m[14];
            kokapena[3] = m[15];

            norabidea[0] = m[8];
            norabidea[1] = m[9];
            norabidea[2] = m[10];

            glLightfv (aim , GL_POSITION , kokapena);
            glLightfv (aim , GL_SPOT_DIRECTION , norabidea);

            /* Fokuaren ezaugarriak */
            glLightf (aim , GL_SPOT_CUTOFF , _light_properties[i]->spot_cutoff);
            glLightf (aim , GL_SPOT_EXPONENT , _light_properties[i]->spot_exp);
            break;

    }

}

/**
 * @brief Argiak ezartzen ditu
 */
void setLights()
{
    int i;
    for (i = 0; i < KG_MAX_LIGHTS; i++)
        setLight(i);
}

/**
 * @brief Argi guztiak pizten ditu.
 */
void enableAllLights()
{
    /* glEnable(GL_LIGHT0); */
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_LIGHT3);
    glEnable(GL_LIGHT4);
}

/**
 * @brief Argi guztiak itzaltzen ditu.
 */
void disableAllLights()
{
    glDisable(GL_LIGHT0);
    glDisable(GL_LIGHT1);
    glDisable(GL_LIGHT2);
    glDisable(GL_LIGHT3);
    glDisable(GL_LIGHT4);
}

/**
 * @brief Argiztapena desgaitzen du.
 */
void disableLighting()
{
    glDisable(GL_LIGHTING);
}

/**
 * @brief Argiztapena gaitzen du.
 */
void enableLighting()
{
    glEnable(GL_LIGHTING);
}

/**
 * @brief Argi bat pizten du.
 * @param num piztu nahi den argia
 */
void enableLight(GLint num)
{
    switch(num)
    {
        case 0:
            glEnable(GL_LIGHT0);
            break;
        case 1:
            glEnable(GL_LIGHT1);
            break;
        case 2:
            glEnable(GL_LIGHT2);
            break;
        case 3:
            glEnable(GL_LIGHT3);
            break;
        case 4:
            glEnable(GL_LIGHT4);
            break;
    }
}

/**
 * @brief Argi bat itzaltzen du.
 * @param num itzaldu nahi den argia
 */
void disableLight(GLint num)
{
    switch(num)
    {
        case 0:
            glDisable(GL_LIGHT0);
            break;
        case 1:
            glDisable(GL_LIGHT1);
            break;
        case 2:
            glDisable(GL_LIGHT2);
            break;
        case 3:
            glDisable(GL_LIGHT3);
            break;
        case 4:
            glDisable(GL_LIGHT4);
            break;
    }
}

/**
 * @brief Argi bat egoeraz aldatzen du.
 * @param num egoeraz aldatu nahi den argia
 */
void toggleLight(GLint num)
{
    switch(num)
    {
        case 0:
            if (glIsEnabled(GL_LIGHT0))
                glDisable(GL_LIGHT0);
            else
                glEnable(GL_LIGHT0);
            break;
        case 1:
            if (glIsEnabled(GL_LIGHT1))
                glDisable(GL_LIGHT1);
            else
                glEnable(GL_LIGHT1);
            break;
        case 2:
            if (glIsEnabled(GL_LIGHT2))
                glDisable(GL_LIGHT2);
            else
                glEnable(GL_LIGHT2);
            break;
        case 3:
            if (glIsEnabled(GL_LIGHT3))
                glDisable(GL_LIGHT3);
            else
                glEnable(GL_LIGHT3);
            break;
        case 4:
            if (glIsEnabled(GL_LIGHT4))
                glDisable(GL_LIGHT4);
            else
                glEnable(GL_LIGHT4);
            break;
    }
}

/**
 * @brief Argi bat piztuta dagoen itzultzen du
 * @param num aztertu nahi den argia
 */
GLint lightIsEnabled(GLint num)
{
    switch(num)
    {
        case 0:
            return glIsEnabled(GL_LIGHT0);
        case 1:
            return glIsEnabled(GL_LIGHT1);
        case 2:
            return glIsEnabled(GL_LIGHT2);
        case 3:
            return glIsEnabled(GL_LIGHT3);
        case 4:
            return glIsEnabled(GL_LIGHT4);
        default:
            return -1;
    }

}

/**
 * @brief Argiztapenaren egoera aldatzen dul.
 */
void toggleLighting()
{
    /* Erabiltzaileak eskatu duen zerbait denez, programak erabaki hori errespetatu dezan */
    _lights_on = !_lights_on;

    if (glIsEnabled(GL_LIGHTING))
        glDisable(GL_LIGHTING);
    else
        glEnable(GL_LIGHTING);
}
