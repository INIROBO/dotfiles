#include <stdio.h>
#include <string.h>
#include <GL/glut.h>
#include "definitions.h"

extern object *_selected_object;

/**
 * @brief Fitxategia irakurtzen du eta materiala sortzeko ezarpenak itzultzen ditu.
 * @param filename fitxategiaren izena.
 * @param settings materiala egiteko jasoko diren balioak
 * @return Irakurritako errenkada kopurua
 */
int read_file(char* filename, GLfloat* settings)
{
    FILE* fp;
    char buf[KG_BUFFER_SIZE];
    char value[KG_BUFFER_SIZE];
    int i;

    if ((fp = fopen(filename, "r")) == NULL)
    {
        /* Open source file. */
        perror("fopen source-file");
        return -1;
    }

    i = 0;
    while (fgets(buf, sizeof(buf), fp) != NULL)
    {
        buf[strlen(buf) - 1] = '\0'; // eat the newline fgets() stores
        if (strlen(buf) > 0)
        {
            settings[i] = atof(buf);
            i++;
        }
    }
    fclose(fp);
    return i;
}

/**
 * @brief Materiala ezartzen dio emandako objektuari.
 * @param obj Objektua
 */
void set_material(object* obj)
{
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];

    glDisable(GL_COLOR_MATERIAL);

    /* Materialaren ezaugarriak jaso */
    /* Aukeratutako objektua bada, pixka bat aldatu beharra dago kolorea */
    if (obj == _selected_object)
    {
        ambient[0] = obj->obj3d->material[0]*KG_SELECTED_OBJECT_MAT_INCR;
        /* Ezin da bat baino handiagoa izan */
        if (ambient[0] > 1.0)
            ambient[0] = 1.0;

        ambient[1] = obj->obj3d->material[1]*KG_SELECTED_OBJECT_MAT_INCR;
        /* Ezin da bat baino handiagoa izan */
        if (ambient[1] > 1.0)
            ambient[1] = 1.0;

        ambient[2] = obj->obj3d->material[2]*KG_SELECTED_OBJECT_MAT_INCR;
        /* Ezin da bat baino handiagoa izan */
        if (ambient[2] > 1.0)
            ambient[2] = 1.0;

        diffuse[0] = obj->obj3d->material[3]*KG_SELECTED_OBJECT_MAT_INCR;
        /* Ezin da bat baino handiagoa izan */
        if (diffuse[0] > 1.0)
            ambient[0] = 1.0;

        diffuse[1] = obj->obj3d->material[4]*KG_SELECTED_OBJECT_MAT_INCR;
        /* Ezin da bat baino handiagoa izan */
        if (diffuse[1] > 1.0)
            diffuse[1] = 1.0;

        diffuse[2] = obj->obj3d->material[5]*KG_SELECTED_OBJECT_MAT_INCR;
        /* Ezin da bat baino handiagoa izan */
        if (diffuse[2] > 1.0)
            diffuse[2] = 1.0;

        specular[0] = obj->obj3d->material[6]*KG_SELECTED_OBJECT_MAT_INCR;
        /* Ezin da bat baino handiagoa izan */
        if (specular[0] > 1.0)
            specular[0] = 1.0;

        specular[1] = obj->obj3d->material[7]*KG_SELECTED_OBJECT_MAT_INCR;
        /* Ezin da bat baino handiagoa izan */
        if (specular[1] > 1.0)
            specular[1] = 1.0;

        specular[2] = obj->obj3d->material[8]*KG_SELECTED_OBJECT_MAT_INCR;
        /* Ezin da bat baino handiagoa izan */
        if (specular[2] > 1.0)
            specular[2] = 1.0;
    }
    else
    {
        ambient[0] = obj->obj3d->material[0];;
        ambient[1] = obj->obj3d->material[1];
        ambient[2] = obj->obj3d->material[2];

        diffuse[0] = obj->obj3d->material[3];
        diffuse[1] = obj->obj3d->material[4];
        diffuse[2] = obj->obj3d->material[5];

        specular[0] = obj->obj3d->material[6];
        specular[1] = obj->obj3d->material[7];
        specular[2] = obj->obj3d->material[8];
    }

    ambient[3] = 1.0;
    diffuse[3] = 1.0;
    specular[3] = 1.0;

    /* Ezarri */
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, obj->obj3d->material[9]);
}
