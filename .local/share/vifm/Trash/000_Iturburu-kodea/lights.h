#ifndef LIGHTS_H
#define LIGHTS_H

void toggleLight(GLint num);
void enableAllLights();
void enableLighting();
void disableLighting();
void setLights();
void toggleLighting();
GLint lightIsEnabled(GLint num);

#endif // LIGHTS_H
