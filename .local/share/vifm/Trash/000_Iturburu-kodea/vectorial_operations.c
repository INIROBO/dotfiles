#include "definitions.h"
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <libgen.h>
#include <math.h>
#include "vectorial_operations.h"

/**
 * @brief Bi punturen kendura kalkulatzen du
 * @param e1 bukaera puntua
 * @param e0 hasiera puntua
 * @return kendurak itzulitako bektorea
 */
vector3 minus(point3 e1, point3 e0)
{
    vector3* v = (vector3*) malloc (sizeof(vector3));

    v->x = e1.x - e0.x;
    v->y = e1.y - e0.y;
    v->z = e1.z - e0.z;

    return *v;
}

/**
 * @brief Bi bektoreen batura arrunta kalkulatzen du
 * @param v0 bektore bat
 * @param v1 beste bektore bat
 * @return bektoreen batura
 */
vector3 normal_sum(vector3 v0, vector3 v1)
{
    vector3 v;

    v.x = v0.x + v1.x;
    v.y = v0.y + v1.y;
    v.z = v0.z + v1.z;

    return v;
}

/**
 * @brief Bi bektoreren x motako biderketa kalkulatzen du
 * @param v0 lehen bektorea
 * @param v1 bigarren bektorea
 * @return bektore berria
 */
vector3 cross(vector3 v0, vector3 v1)
{
    vector3* v = (vector3*) malloc (sizeof(vector3));

    v->x = v0.y*v1.z - v0.z*v1.y;
    v->y = v0.z*v1.x - v0.x*v1.z;
    v->z = v0.x*v1.y - v0.y*v1.x;

    return *v;
}

/**
 * @brief bektore baten modulua kalkulatzen du
 * @param v0 bektorea
 * @return modulua
 */
GLdouble magnitude(vector3 v0)
{
    return sqrt(pow(v0.x,2) + pow( v0.y,2 ) + pow(v0.z,2));
}

/**
 * @brief Bektore eta zenbaki baten arteko zatidura
 * @param v0 bektorea
 * @param m zenbakia
 * @return zatidura
 */
vector3 division(vector3 v0, GLdouble m)
{
    vector3* v = (vector3*) malloc (sizeof(vector3));

    v->x = v0.x/m;
    v->y = v0.y/m;
    v->z = v0.z/m;

    return *v;
}

/**
 * @brief Bektore bat irteera estandarrean bistaratzen du
 * @param v bektorea
 */
void showVector(vector3 v)
{
    printf("vector: (%f, %f, %f)\n", v.x, v.y, v.z);
}

/**
 * @brief Puntu bat irteera estandarrean bistaratzen du
 * @param p puntua
 */
void showPoint(point3 p)
{
    printf("(%f, %f, %f)\n", p.x, p.y, p.z);
}
