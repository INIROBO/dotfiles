#include "definitions.h"
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include "matrizeak.h"
#include "materials.h"
#include "lights.h"

/** EXTERNAL VARIABLES **/

extern GLdouble _window_ratio;
extern GLdouble _ortho_x_min,_ortho_x_max;
extern GLdouble _ortho_y_min,_ortho_y_max;
extern GLdouble _ortho_z_min,_ortho_z_max;

extern object *_first_object;
extern object *_selected_object;
extern transformation_history* _last_transformation;

extern GLint _camera_type;
extern object * _perspective_camera;
extern object * _moving_camera;
extern GLdouble* _moving_camera_orient_matrix;

extern GLdouble _perspective_camera_fovy;
extern GLdouble _moving_camera_fovy;

extern GLint _lights_on;

/**
 * @brief Kamara ezartzen du
 * @param direction Noranzko
 * @return Transformazio matrizea
 */
void set_cam()
{
    GLdouble* matrix = 0;

    switch(_camera_type)
    {

        case KG_PERSPECTIVE_CAM:
            matrix = multiplyM(persCamInitialECUMatrix(), _perspective_camera->transformations_last->matrix);
            gluLookAt(matrix[0], matrix[1], matrix[2],
                    matrix[4], matrix[5], matrix[6],
                    matrix[8], matrix[9], matrix[10]);

            break;

        case KG_MOVING_CAM:
            matrix = multiplyM(_moving_camera_orient_matrix, _moving_camera->transformations_last->matrix);
            matrix = multiplyM(movingCamInitialECUMatrix(), matrix);
            gluLookAt(matrix[0], matrix[1], matrix[2],
                    matrix[4], matrix[5], matrix[6],
                    matrix[8], matrix[9], matrix[10]);

            break;

    }

}

/**
 * @brief Function to draw the axes
 */
void draw_axes()
{
    /*Draw X axis*/
    glColor3f(KG_COL_X_AXIS_R,KG_COL_X_AXIS_G,KG_COL_X_AXIS_B);
    glBegin(GL_LINES);
    glVertex3d(KG_MAX_DOUBLE,0,0);
    glVertex3d(-1*KG_MAX_DOUBLE,0,0);
    glEnd();
    /*Draw Y axis*/
    glColor3f(KG_COL_Y_AXIS_R,KG_COL_Y_AXIS_G,KG_COL_Y_AXIS_B);
    glBegin(GL_LINES);
    glVertex3d(0,KG_MAX_DOUBLE,0);
    glVertex3d(0,-1*KG_MAX_DOUBLE,0);
    glEnd();
    /*Draw Z axis*/
    glColor3f(KG_COL_Z_AXIS_R,KG_COL_Z_AXIS_G,KG_COL_Z_AXIS_B);
    glBegin(GL_LINES);
    glVertex3d(0,0,KG_MAX_DOUBLE);
    glVertex3d(0,0,-1*KG_MAX_DOUBLE);
    glEnd();
}
/**
 * @brief Function to draw the grid
 */
void draw_grid()
{

    /** Draw x axis parallel grid lines **/
    glColor3f(KG_COL_GRID_R,KG_COL_GRID_G,KG_COL_GRID_B);
    glBegin(GL_LINES);
    double i;
    for (i = -1*KG_MAX_DOUBLE; i < KG_MAX_DOUBLE; i += KG_DISTANCE_GRID_Y)
    {
        if (_camera_type == KG_ORTHO_CAM)
        {
            glVertex3d(KG_MAX_DOUBLE, i, 0);
            glVertex3d(-1*KG_MAX_DOUBLE, i, 0);
        }
        else
        {
            glVertex3d(KG_MAX_DOUBLE, 0, i);
            glVertex3d(-1*KG_MAX_DOUBLE, 0, i);
        }
    }
    glEnd();

    /** Draw y axis parallel grid lines **/
    glColor3f(KG_COL_GRID_R,KG_COL_GRID_G,KG_COL_GRID_B);
    glBegin(GL_LINES);
    double j;
    for (j = -1*KG_MAX_DOUBLE; j < KG_MAX_DOUBLE; j += KG_DISTANCE_GRID_X)
    {
        if (_camera_type == KG_ORTHO_CAM)
        {
            glVertex3d(j, KG_MAX_DOUBLE, 0);
            glVertex3d(j, -1*KG_MAX_DOUBLE, 0);
        }
        else
        {
            glVertex3d(j, 0, KG_MAX_DOUBLE);
            glVertex3d(j, 0, -1*KG_MAX_DOUBLE);
        }
    }
    glEnd();

}


/**
 * @brief Callback reshape function. We just store the new proportions of the window
 * @param width New width of the window
 * @param height New height of the window
 */
void reshape(int width, int height) {
    glViewport(0, 0, width, height);
    /*  Take care, the width and height are integer numbers, but the ratio is a GLdouble so, in order to avoid
     *  rounding the ratio to integer values we need to cast width and height before computing the ratio */
    _window_ratio = (GLdouble) width / (GLdouble) height;
}


/**
 * @brief Callback display function
 */
void display(void) {
    GLint v_index, v, f;
    object *aux_obj = _first_object;

    /* Clear the screen */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* Define the projection */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();


    switch(_camera_type)
    {

        case KG_ORTHO_CAM:
            /*When the window is wider than our original projection plane we extend the plane in the X axis*/
            if ((_ortho_x_max - _ortho_x_min) / (_ortho_y_max - _ortho_y_min) < _window_ratio) {
                /* New width */
                GLdouble wd = (_ortho_y_max - _ortho_y_min) * _window_ratio;
                /* Midpoint in the X axis */
                GLdouble midpt = (_ortho_x_min + _ortho_x_max) / 2;
                /*Definition of the projection*/
                glOrtho(midpt - (wd / 2), midpt + (wd / 2), _ortho_y_min, _ortho_y_max, _ortho_z_min, _ortho_z_max);
            } else {/* In the opposite situation we extend the Y axis */
                /* New height */
                GLdouble he = (_ortho_x_max - _ortho_x_min) / _window_ratio;
                /* Midpoint in the Y axis */
                GLdouble midpt = (_ortho_y_min + _ortho_y_max) / 2;
                /*Definition of the projection*/
                glOrtho(_ortho_x_min, _ortho_x_max, midpt - (he / 2), midpt + (he / 2), _ortho_z_min, _ortho_z_max);
            }
            break;

        case KG_PERSPECTIVE_CAM:
            gluPerspective(_perspective_camera_fovy,  _window_ratio,  KG_CAM_ZNEAR,  KG_CAM_ZFAR);
            break;

        case KG_MOVING_CAM:
            gluPerspective(_moving_camera_fovy,  _window_ratio,  KG_CAM_ZNEAR,  KG_CAM_ZFAR);
            break;

    }


    /* Now we start drawing the object */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /* Kamara kargatu */
    set_cam();

    /* Set Lights */
    setLights();

    /* Erabiltzaileak argiztapena nola egon dadin egindako erabakia errespetatzeko */
    if (_lights_on)
        disableLighting();

    /* Firstly, we draw the axes*/
    draw_axes();

    /* Then, we draw the grid */
    draw_grid();

    /* Erabiltzaileak argiztapena nola egon dadin egindako erabakia errespetatzeko */
    if (_lights_on)
        enableLighting();


    /*Now, for each of the objects in the list*/
    while (aux_obj != 0) {
        /* Select the color, depending on whether the current object is the selected one or not */
        if (aux_obj == _selected_object){
            glColor3f(KG_COL_SELECTED_R,KG_COL_SELECTED_G,KG_COL_SELECTED_B);
        }else{
            glColor3f(KG_COL_NONSELECTED_R,KG_COL_NONSELECTED_G,KG_COL_NONSELECTED_B);
        }

        /* Materialik badu */
        if (aux_obj->obj3d->material != 0)
        {
            set_material(aux_obj);
        }



        /* Draw the object; for each face create a new polygon with the corresponding vertices */
        glLoadIdentity();
        set_cam();
        /* Get the first transformation of the current object in order to go through all of the rest. */
        transformation_history* transf = aux_obj->transformations_last;
        if (transf != 0 && aux_obj->num_transformations > 0)
        {
            GLdouble* m = transf->matrix;
            glMultMatrixd(m);
        }

        for (f = 0; f < aux_obj->obj3d->num_faces; f++) {
            glBegin(GL_POLYGON);

            /* Objektuak leuntasuna izan beharra ez badauka */
            if (!aux_obj->obj3d->smooth)
                glNormal3d(aux_obj->obj3d->face_table[f].normal.x, aux_obj->obj3d->face_table[f].normal.y, aux_obj->obj3d->face_table[f].normal.z);

            for (v = 0; v < aux_obj->obj3d->face_table[f].num_vertices; v++) {
                v_index = aux_obj->obj3d->face_table[f].vertex_table[v];

                /* Objektuak leuntasuna izan beharra badauka */
                if (aux_obj->obj3d->smooth)
                    glNormal3d(aux_obj->obj3d->vertex_table[v_index].normal.x, aux_obj->obj3d->vertex_table[v_index].normal.y, aux_obj->obj3d->vertex_table[v_index].normal.z);

                glVertex3d(aux_obj->obj3d->vertex_table[v_index].coord.x,
                        aux_obj->obj3d->vertex_table[v_index].coord.y,
                        aux_obj->obj3d->vertex_table[v_index].coord.z);

            }
            glEnd();
        }


        aux_obj = aux_obj->next;
    glEnable(GL_COLOR_MATERIAL);
    }
    /*Do the actual drawing*/
    glFlush();
}
