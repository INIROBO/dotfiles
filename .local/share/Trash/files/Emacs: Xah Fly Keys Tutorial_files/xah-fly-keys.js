
{

    const reverse_obj_key_val = ((obj) => {
        // given a obj, return a new obj that has its key and value reversed. If values are not unique, the previous is overwritten. 2018-06-04
        const result = {};
        Object.keys ( obj ). forEach ( ((x) => { result[obj[x]] = x }));
        return result;
    });

    const dvorak_to_qwerty_table = {
        "." : "e",
        "," : "w",
        "'" : "q",
        ";" : "z",
        "/" : "[",
        "[" : "-",
        "]" : "=",
        "=" : "]",
        "-" : "'",

        "a" : "a",
        "b" : "n",
        "c" : "i",
        "d" : "h",
        "e" : "d",
        "f" : "y",
        "g" : "u",
        "h" : "j",
        "i" : "g",
        "j" : "c",
        "k" : "v",
        "l" : "p",
        "m" : "m",
        "n" : "l",
        "o" : "s",
        "p" : "r",
        "q" : "x",
        "r" : "o",
        "s" : ";",
        "t" : "k",
        "u" : "f",
        "v" : ".",
        "w" : ",",
        "x" : "b",
        "y" : "t",
        "z" : "/",

    };

    const qwerty_to_dvorak_table = reverse_obj_key_val(dvorak_to_qwerty_table);

    const key_dvorak_to_qwerty = ((x) => {
        const result = dvorak_to_qwerty_table[x];
        return ( ( result === undefined ) ? x : result );
    });

    const key_qwerty_to_dvorak = ((x) => {
        const result = qwerty_to_dvorak_table[x];
        return ( ( result === undefined ) ? x : result );
    });

    const currentLayout = (() => {
        const keyClassList = document.querySelector ( 'kbd.key' ) .classList;

        if ( keyClassList .contains ("qwerty") ) {
            return "qwerty";
        } else if ( keyClassList .contains ("dvorak") )
        { return "dvorak"; }
        else { return "error"; }
    });

    const changePicToQwerty = (() => {
        document.getElementById ("qwerty_pic").style.display="inline";
        document.getElementById ("dvorak_pic").style.display="none";
    });

    const changePicToDvorak = (() => {
        document.getElementById ("qwerty_pic").style.display="none";
        document.getElementById ("dvorak_pic").style.display="inline";
    });

    const changeToQwerty = (() => {
        if ( currentLayout() === "qwerty" ) { }
        else {
            const things = document.querySelectorAll ( 'kbd.key' );
            for (let i = 0; i < things.length; i++) {
                const el = things[i];
                const kk = el. textContent;
                if ( el.classList.contains("qwerty") ) {
                    el. textContent = ( key_qwerty_to_dvorak(kk) );
                    el.classList.remove("qwerty");
                    el.classList.add("dvorak")
                } else {
                    el. textContent = ( key_dvorak_to_qwerty(kk) );
                    el.classList.remove("dvorak");
                    el.classList.add("qwerty");
                }
            };
            changePicToQwerty();
        }});

    const changeToDvorak = (() => {
        if ( currentLayout() === "dvorak" ) {} 
        else {
            const things = document.querySelectorAll ( 'kbd.key' );
            for (let i = 0; i < things.length; i++) {
                const el = things[i];
                const kk = el. textContent;

                if ( el.classList.contains("qwerty") ) {
                    el. textContent = ( key_qwerty_to_dvorak(kk) );
                    el.classList.remove("qwerty");
                    el.classList.add("dvorak")
                } else {
                    el. textContent = ( key_dvorak_to_qwerty(kk) );
                    el.classList.remove("dvorak");
                    el.classList.add("qwerty");
                }
            };
            changePicToDvorak();
        } });

    document. getElementById ('button_qw'). addEventListener ('click', changeToQwerty, false);
    document. getElementById ('button_dv'). addEventListener ('click', changeToDvorak, false);

}
