"call plug#begin('~/.vim/plugged')

" For LaTeX
Plug 'vim-latex/vim-latex'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary' " Commentary.vim
Plug 'tpope/vim-surround'
Plug 'itchyny/lightline.vim'
Plug 'michaeljsmith/vim-indent-object'
Plug 'Reewr/vim-monokai-phoenix'
" Plug 'rafi/awesome-vim-colorschemes'
Plug 'terryma/vim-multiple-cursors'
Plug 'scrooloose/nerdtree'
Plug 'hdima/python-syntax' 
Plug 'mileszs/ack.vim'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-easytags'
Plug 'morhetz/gruvbox'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'vim-scripts/ReplaceWithRegister'
Plug 'christoomey/vim-system-copy'
Plug 'python-mode/python-mode', { 'branch': 'develop' }
" Pending tasks list
" Plug 'fisadev/FixedTaskList.vim' ", { 'for': 'python' }
" " Python and other languages code checker
" Plug 'scrooloose/syntastic' ", { 'for': 'python' }
" " Indent text object
" Plug 'michaeljsmith/vim-indent-object' ", { 'for': 'python' }
" " Indentation based movements
" Plug 'jeetsukumaran/vim-indentwise' ", { 'for': 'python' }
" Python autocompletion, go to definition.
"Plug 'davidhalter/jedi-vim', { 'for': 'python' }
" " Better autocompletion
" Plug 'Shougo/neocomplcache.vim' ", { 'for': 'python' }

call plug#end()
	

" Settings

" LaTeX
let g:tex_flavor='latex'

" Numbers
:set number
:set relativenumber

" Color scheme + syntax
syntax on
:colorscheme monokai-phoenix

" CUSTOM CONFIG
" Search down in subforlders
 set path+=**
 set nocompatible
 set encoding=utf-8
 syntax enable
 filetype on
 filetype plugin indent on
 set autoindent
 set laststatus=2
 set incsearch
 set hlsearch
 set cursorline

" Didsplay all atvhing files when we tab complete
 set wildmenu

map <C-p> <C-]>

" TAGS
 " Create the 'tags' file (may need to install ctags first)
"let g:autotagTagsFile=".tags"
 " JAVA
" command! MakeTags !ctags -R .

 " C
set termguicolors 
let mapleader=","


" tab navigation mappings
map tn :tabn<CR>
map tp :tabp<CR>
map tm :tabm
map tt :tabnew
map ts :tab split<CR>
map <C-S-Right> :tabn<CR>
imap <C-S-Right> <ESC>:tabn<CR>
map <C-S-Left> :tabp<CR>
imap <C-S-Left> <ESC>:tabp<CR>

" navigate windows with meta+arrows
map <M-Right> <c-w>l
map <M-Left> <c-w>h
map <M-Up> <c-w>k
map <M-Down> <c-w>j
imap <M-Right> <ESC><c-w>l
imap <M-Left> <ESC><c-w>h
imap <M-Up> <ESC><c-w>k
imap <M-Down> <ESC><c-w>j


" better backup, swap and undos storage
set directory=~/.vim/dirs/tmp     " directory to place swap files in
set backup                        " make backup files
set backupdir=~/.vim/dirs/backups " where to put backup files
set undofile                      " persistent undos - undo after you re-open the file
set undodir=~/.vim/dirs/undos
set viminfo+=n~/.vim/dirs/viminfo
" store yankring history file there too
let g:yankring_history_dir = '~/.vim/dirs/'

" create needed directories if they don't exist
if !isdirectory(&backupdir)
    call mkdir(&backupdir, "p")
endif
if !isdirectory(&directory)
    call mkdir(&directory, "p")
endif
if !isdirectory(&undodir)
    call mkdir(&undodir, "p")
endif

" toggle nerdtree display
map <F3> :NERDTreeToggle<CR>
" open nerdtree with the current file selected
nmap <leader>t :NERDTreeFind<CR>
" don;t show these file types
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']


" Lo de los brackets y parentesis
inoremap ( ()<Esc>i
inoremap [ []<Esc>i
inoremap { {<CR>}<Esc>O
autocmd Syntax html,vim inoremap < <lt>><Esc>i| inoremap > <c-r>=ClosePair('>')<CR>
inoremap ) <c-r>=ClosePair(')')<CR>
inoremap ] <c-r>=ClosePair(']')<CR>
inoremap } <c-r>=CloseBracket()<CR>
inoremap " <c-r>=QuoteDelim('"')<CR>
inoremap ' <c-r>=QuoteDelim("'")<CR>

function ClosePair(char)
 if getline('.')[col('.') - 1] == a:char
 return "\<Right>"
 else
 return a:char
 endif
endf

function CloseBracket()
 if match(getline(line('.') + 1), '\s*}') < 0
 return "\<CR>}"
 else
 return "\<Esc>j0f}a"
 endif
endf

function QuoteDelim(char)
 let line = getline('.')
 let col = col('.')
 if line[col - 2] == "\\"
 "Inserting a quoted quotation mark into the string
 return a:char
 elseif line[col - 1] == a:char
 "Escaping out of the string
 return "\<Right>"
 else
 "Starting a string
 return a:char.a:char."\<Esc>i"
 endif
endf


" tab length exceptions on some file types
autocmd FileType html setlocal shiftwidth=4 tabstop=4 softtabstop=4
autocmd FileType htmldjango setlocal shiftwidth=4 tabstop=4 softtabstop=4
autocmd FileType javascript setlocal shiftwidth=4 tabstop=4 softtabstop=4


