call plug#begin('~/.vim/plugged')

" For LaTeX
Plug 'vim-latex/vim-latex'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary' " Commentary.vim
Plug 'tpope/vim-surround'

Plug 'michaeljsmith/vim-indent-object'
Plug 'Reewr/vim-monokai-phoenix'
" Plug 'rafi/awesome-vim-colorschemes'
Plug 'terryma/vim-multiple-cursors'
Plug 'scrooloose/nerdtree'
Plug 'hdima/python-syntax' 
Plug 'mileszs/ack.vim'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-easytags'
Plug 'morhetz/gruvbox'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'vim-scripts/ReplaceWithRegister'
Plug 'christoomey/vim-system-copy'

call plug#end()


" Settings

" LaTeX
let g:tex_flavor='latex'

" Numbers
:set number
:set relativenumber

" Color scheme + syntax
syntax on
:colorscheme monokai-phoenix



" CUSTOM CONFIG
" Search down in subforlders
 set path+=**

" Didsplay all atvhing files when we tab complete
 set wildmenu

map <C-p> <C-]>

" TAGS
 " Create the 'tags' file (may need to install ctags first)
"let g:autotagTagsFile=".tags"
 " JAVA
" command! MakeTags !ctags -R .

 " C
set termguicolors 
let mapleader=","

