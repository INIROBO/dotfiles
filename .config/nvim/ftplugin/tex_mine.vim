" LaTeX                                                                    
map <buffer> <C-m> :!mupdf %:p:r.pdf<ENTER>
map <buffer> <C-l> :!pdflatex -output-directory %:p:h %:p<ENTER>

