#include "definitions.h"
#include "load_obj.h"
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include "matrizeak.h"
#include "display.h"
#include "materials.h"

extern object * _first_object;
extern object * _selected_object;
extern int _transformation_objective;
extern int _transformation;

extern GLdouble _ortho_x_min,_ortho_x_max;
extern GLdouble _ortho_y_min,_ortho_y_max;
extern GLdouble _ortho_z_min,_ortho_z_max;

extern GLdouble _perspective_camera_fovy;
extern GLdouble _moving_camera_fovy;

extern GLint _camera_type;
extern object * _perspective_camera;
extern object * _moving_camera;

extern GLdouble* _moving_camera_orient_matrix;

extern object * _object_to_transform;

extern GLint _lights_on;

extern object* _light[KG_MAX_LIGHTS];
extern GLint _selected_light;
extern light_properties* _light_properties[KG_MAX_LIGHTS];

/**
 * @brief This function just prints information about the use
 * of the keys
 */
void print_help(){
    printf("KbG Irakasgaiaren Praktika. Programa honek 3D objektuak \n");
    printf("aldatzen eta bistaratzen ditu.  \n\n");

    printf("Egilea: Inigo Ortega (iortega045@ikasle.ehu.eus) \n");
    printf("Data: Abenduak 23, 2018 \n");
    printf("\n\n");
    printf("FUNTZIO NAGUSIAK \n");
    printf("<?>\t\t Laguntza hau bistaratu \n");
    printf("<ESC>\t\t Programatik irten \n");
    printf("<F>\t\t Objektua bat kargatu\n");
    printf("<TAB>\t\t Kargaturiko objektuen artean bat hautatu\n");
    printf("<DEL>\t\t Hautatutako objektua ezabatu\n");
    printf("<CTRL + ->\t Bistaratze-eremua handitu\n");
    printf("<CTRL + +>\t Bistaratze-eremua txikitu\n");
    printf("<I>\t\t Aukeratutako objektuaren informazioa inprimatu\n");
    printf("<CTRL + M>\t Aukeratutako objektuaren azken transformazio matrizea ikusi\n");
    printf("\n\n");
    printf("TRANSFORMAZIO FUNTZIOAK \n");
    printf("<M>\t\t Translazioa aktibatu \n");
    printf("<B>\t\t Biraketa aktibatu \n");
    printf("<T>\t\t Tamaina aldaketa aktibatu \n");
    printf("<R>\t\t Islapena aktibatu \n");
    printf("<G>\t\t Transformazio globalak aktibatu \n");
    printf("<L>\t\t Transformazio lokalak aktibatu \n");
    printf("<Q>\t\t AvPag simulatu \n");
    printf("<W>\t\t RePag simulatu \n");
    printf("<Ezker gezia>\t Mugitu -X; Handitu X; Biratu -Y; Isladatu X \n");
    printf("<Eskuin gezia>\t Mugitu +X; Txikitu X; Biratu +Y; Isladatu X \n");
    printf("<Gora gezia>\t Mugitu +Y; Txikitu Y; Biratu +X; Isladatu Y \n");
    printf("<Behera gezia>\t Mugitu -Y; Handitu Y; Biratu -X; Isladatu Y \n");
    printf("<AvPag>\t\t Mugitu +Z; Txikitu Z; Biratu +Z; Isladatu Z \n");
    printf("<RePag>\t\t Mugitu -Z; Handitu Z; Biratu -Z; Isladatu Z \n");
    printf("\n\n");
    printf("KAMARAK \n");
    printf("<C>\t\t Kamaraz aldatu\n");
    printf("<K>\t\t Uneko kamara aukeratu\n");
    printf("<O>\t\t Transformaziaok aukeratutako objektuari egin\n");
    printf("\n");
    printf("Kamara ortografikoa:\n");
    printf("Defektuzko kamara. Ezin da mugitu, beti dago toki berean.\n");
    printf("\n");
    printf("Kamara perspektiboa:\n");
    printf("Espazioan edozein tokitik mugitu eta edonora begira dezake.\nObjektuetarako transformazio agindu berberak daude erabilgarri.\n");
    printf("\n");
    printf("Kamara ibilitaria:\n");
    printf("X = 2 planoan mugi daiteke bakarrik. Ondorioz, kamararen mugimendua honelakoa da:\n");
    printf("<Ezker gezia>\t Ezkerrera biratu. \n");
    printf("<Eskuin gezia>\t Eskubira biratu. \n");
    printf("<Gora gezia>\t Aurrera mugitu.\n");
    printf("<Behera gezia>\t Atzera mugitu.\n");
    printf("<AvPag>\t\t Gora biguratu.\n");
    printf("<RePag>\t\t Behera begiratu.\n");
    printf("\n\n");
    printf("ARGIAK\n");
    printf("<Enter>\t\t Argiztatzea gaitu/desgaitu\n");
    printf("<1>...<5>\t Argiak aukeratu (1 1. aukeratzeko, 2 2. aukeratzeko etab.)\n");
    printf("<F1>...<F5>\t     Argiak piztu/itzali (F1 1. pizteko/itzaltzeko, F2 2. pizteko/itzaltzeko etab.)\n");
    printf("<0>\t\t Argi mota aldatu\n");
    printf("<A>\t\t Aukeratutako argiari aplikatu transformazioak\n");
    printf("<+>\t\t Foku motako argi bat aukeraturik badago, bere angelua handitu\n");
    printf("<->\t\t Foku motako argi bat aukeraturik badago, bere angelua txikitu\n");
    printf("<F11>\t\t Hautaturik dagoen objektuari materiala kargatu, fitxategi batetik irakurrita\n");
    printf("<F12>\t\t Hautaturik dagoen objektua bistaratzeko era (flat / smooth) aldatu\n");
    printf("<S>\t\t argiari buruzko informazioa inprimatu\n");
    printf("\n");
}

/**
 * @brief Objektu bat, nolako transformazioa izango duen esaten duen noranzkoa eta transformazio funtzioa emanda, objektua transformatzen du eta behar diren aldaketak egiten dira objektuan transformazio hori gordetzeko.
 * @param (*f)(float) transformazio funtzioa
 * @param direction Noranzkoa
 * @param object Transformatuo den objektua
 */
void transform( GLdouble* (*f)(float), float direction , object* obj)
{
    if (obj != NULL)
    {


        GLdouble* m = 0;
        GLdouble* mResult = 0;
        float x, y, z;

        /* Transformazio matrizea lortu. */
        m = (*f)(direction);
        //showMatrix(m);


        /* Transformazioen historiala eguneratu */
        /* Lehen transformazioa bada eta gorputz fisikoa badu*/
        if (obj->num_transformations == 0 && obj->obj3d != 0)
        {
            /* Sortu lehen transformazioa, prestatu objektua hurrengo transformazioetarako eta historialean sartu. */
            obj->transformations_last = (transformation_history *) malloc(sizeof (transformation_history));
            obj->transformations_first = obj->transformations_last;
            obj->transformations_last->previous = 0;

            /* Esleitu lehen matrizea */
            mResult = m;
        }
        else
        {
            /* Translazio bektorea */
            x = obj->transformations_last->matrix[12]; 
            y = obj->transformations_last->matrix[13]; 
            z = obj->transformations_last->matrix[14]; 

            /* Lehen transformazioa ez bada, transformazio historialean gehitu eta prestatu erabilgarria izateko. */
            obj->transformations_last->next = (transformation_history *) malloc(sizeof (transformation_history));
            obj->transformations_last->next->previous = obj->transformations_last;
            obj->transformations_last = obj->transformations_last->next;
            obj->transformations_last->next = 0;

            //showMatrix(object->transformations_last->previous->matrix);

            /* Transformazio globalean egoeran bagaude, */
            if (_transformation_objective == KG_GLOBAL_TRANSFORMATION)
            {
                mResult = multiplyM(obj->transformations_last->previous->matrix, m);
            }
            else
            {
                /* Alderantziz. */
                mResult = multiplyM(m, obj->transformations_last->previous->matrix);
            }
        }

        /* Transformazio kopurua handitu. */
        obj->num_transformations++;

        /* Transformazioen historialeko askeneko elementuan esleitu dagokion matrizea. */
        obj->transformations_last->matrix = mResult;

    }

}

int disableNonSafeFunctionalities(object* obj)
{
    if (obj->obj3d == 0)
    {
        /* Tamaina aldatzeko transformazioek ez dute zentzurik kamarekin eta argiekin*/
        if (_transformation == KG_MODIFY)
            return 1;
        else
            if (obj->state == KG_LIGHT && _selected_light == KG_DIRECTIONAL_LIGHT && _transformation == KG_TRANSLATE)
                return 1;
    }

    return 0;
}

/**
 * @brief RePag sakatzean egin beharrekoa.
 */
void page_up(object* obj)
{
    if (disableNonSafeFunctionalities(obj))
        return;
    else
        if (_camera_type == KG_MOVING_CAM && obj->obj3d == 0)
        {
            _moving_camera_orient_matrix = multiplyM(rotationY(-1), _moving_camera_orient_matrix);
            return;
        }
    switch (_transformation)
    {
        case KG_TRANSLATE:
            transform(translationZ, -1, obj);
            break;

        case KG_ROTATE:
            transform(rotationZ, 1, obj);
            break;

        case KG_MODIFY:
            transform(modificationZ, 2, obj);
            break;

        case KG_REFLECT:
            transform(reflectZ, 1, obj);
            break;
    }




}

/**
 * @brief AvPag sakatzean egin beharrekoa.
 */
void page_down(object* obj)
{
    if (disableNonSafeFunctionalities(obj))
        return;
    else
        if (_camera_type == KG_MOVING_CAM && obj->obj3d == 0)
        {
            _moving_camera_orient_matrix = multiplyM(rotationY(1), _moving_camera_orient_matrix);
            return;
        }
    switch (_transformation)
    {
        case KG_TRANSLATE:
            transform(translationZ, 1, obj);
            break;

        case KG_ROTATE:
            transform(rotationZ, -1, obj);
            break;

        case KG_MODIFY:
            transform(modificationZ, 0.5, obj);
            break;

        case KG_REFLECT:
            transform(reflectZ, 1, obj);
            break;
    }


}

/**
 * @brief Eskuineko gezia sakatzean egin beharrekoa.
 */
void key_right(object* obj)
{
    if (disableNonSafeFunctionalities(obj))
        return;
    else
        if (_camera_type == KG_MOVING_CAM && obj->obj3d == 0)
        {
            transform(rotationX, 1, obj);
            return;
        }
    switch (_transformation)
    {
        case KG_TRANSLATE:
            transform(translationX, 1, obj);
            break;

        case KG_ROTATE:
            transform(rotationX, 1, obj);
            break;

        case KG_MODIFY:
            transform(modificationX, 0.5, obj);
            break;

        case KG_REFLECT:
            transform(reflectX, 1, obj);
            break;
    }

}

/**
 * @brief Ezkerreko gezia sakatzean egin beharrekoa.
 */
void key_left(object* obj)
{
    if (disableNonSafeFunctionalities(obj))
        return;
    else
        if (_camera_type == KG_MOVING_CAM && obj->obj3d == 0)
        {
            transform(rotationX, -1, obj);
            return;
        }
    switch (_transformation)
    {
        case KG_TRANSLATE:
            transform(translationX, -1, obj);
            break;

        case KG_ROTATE:
            transform(rotationX, -1, obj);
            break;

        case KG_MODIFY:
            transform(modificationX, 2, obj);
            break;

        case KG_REFLECT:
            transform(reflectX, 1, obj);
            break;
    }

}

/**
 * @brief Gorako gezia sakatzean egin beharrekoa.
 */
void key_up(object* obj)
{
    if (disableNonSafeFunctionalities(obj))
        return;
    else
        if (_camera_type == KG_MOVING_CAM && obj->obj3d == 0)
        {
            transform(translationZ, -1, obj);
            return;
        }
    switch (_transformation)
    {
        case KG_TRANSLATE:
            transform(translationY, 1, obj);
            break;

        case KG_ROTATE:
            transform(rotationY, 1, obj);
            break;

        case KG_MODIFY:
            transform(modificationY, 0.5, obj);
            break;

        case KG_REFLECT:
            transform(reflectY, 1, obj);
            break;
    }

}

/**
 * @brief Beherako gezia sakatzean egin beharrekoa.
 */
void key_down(object* obj)
{
    if (disableNonSafeFunctionalities(obj))
        return;
    else
        if (_camera_type == KG_MOVING_CAM && obj->obj3d == 0)
        {
            transform(translationZ, 1, obj);
            return;
        }
    switch (_transformation)
    {
        case KG_TRANSLATE:
            transform(translationY, -1, obj);
            break;

        case KG_ROTATE:
            transform(rotationY, -1, obj);
            break;

        case KG_MODIFY:
            transform(modificationY, 2, obj);
            break;

        case KG_REFLECT:
            transform(reflectY, 1, obj);
            break;
    }

}

/**
 * @brief Azken aldaketa desegiteko funtzioa.
 * @param obj azken aldaketa desegiteko zaion objektua.
 */
void undo(object* obj)
{
    if (obj->num_transformations > 0)
    {

        /* Transformazio historiala aldatzeko: */
        /* Transformazio bakarra egon ez bada, */
        if (obj->transformations_last->previous != 0)
        {
            /* Azken ezabatu. */
            obj->transformations_last = obj->transformations_last->previous;
        }


        /* Gutxitu transformazio kopurua. */
        obj->num_transformations--;
    }
}

/**
 * @brief Azken desegitean ezabatutako aldaketa berregiteko.
 * @param obj aldaketak berreskuratuko dituen objektua.
 */
void redo(object* obj)
{
    /* Ez badago transformaziorik edo objekturik ez badago, */
    if (obj == NULL || obj->transformations_last == NULL)
    {
        return;
    }
    /* Transformaziorik ez bado, ezin da hurrengoa esleitu, aurreko baldintza igaro delako, orduan,
       behintzat transformazio bat eskutatuta dago eta hori num_transformations handituta jarriko da ikusgai */
    if (obj->num_transformations > 0)
        /* Transformazioak badauzka ezabatuta, */
        if (obj->transformations_last->next != 0)
        {
            obj->transformations_last = obj->transformations_last->next;
        }

    obj->num_transformations++;
}

/**
 * @brief Callback function to control the special keys
 * @param key Key that has been pressed
 * @param x X coordinate of the mouse pointer when the key was pressed
 * @param y Y coordinate of the mouse pointer when the key was pressed
 */
void special_keyboard(int key, int x, int y) {

    GLfloat settings[KG_VALUE_MAX_LENGTH];
    char filename[KG_BUFFER_SIZE];
    int i;
    int lines;

    switch (key) {
        case GLUT_KEY_RIGHT:
            key_right(_object_to_transform);
            glutPostRedisplay();
            break;

        case GLUT_KEY_LEFT:

            key_left(_object_to_transform);
            glutPostRedisplay();
            break;

        case GLUT_KEY_UP:

            key_up(_object_to_transform);
            glutPostRedisplay();
            break;

        case GLUT_KEY_DOWN:

            key_down(_object_to_transform);
            glutPostRedisplay();
            break;

        case GLUT_KEY_PAGE_UP:

            page_up(_object_to_transform);
            glutPostRedisplay();
            break;

        case GLUT_KEY_PAGE_DOWN:

            page_down(_object_to_transform);
            glutPostRedisplay();
            break;

        case GLUT_KEY_F1:
        case GLUT_KEY_F2:
        case GLUT_KEY_F3:
        case GLUT_KEY_F4:
        case GLUT_KEY_F5:
            /* Argiak itzli/piztu */
            toggleLight(key - 1);
            glutPostRedisplay();
            break;

        case GLUT_KEY_F11:
            /* Fitxategia jasotzeko eskaera */
            printf(KG_MSSG_SELECT_FILE);
            scanf("%s", filename);

            /* Jaso ezaugarriak testu fitxategitik */
            if ((lines = read_file(filename, settings)) == 10)
            {
                _selected_object->obj3d->material = (GLfloat*) malloc (KG_VALUE_MAX_LENGTH*sizeof(GLfloat));

                /* Idatzi ezaugarriak */
                for (i = 0; i < KG_VALUE_MAX_LENGTH; i++)
                    _selected_object->obj3d->material[i] = settings[i];
                glutPostRedisplay();
            }
            else
            {
                printf("%s. lines %d\n", KG_MSSG_INVALIDFILE, lines);
            }
            break;

        case GLUT_KEY_F12:
            /* Leuntasun mota aukeratu */
            if (_selected_object->obj3d != 0)
            {
                if (_selected_object->obj3d->smooth)
                    _selected_object->obj3d->smooth = 0;
                else
                    _selected_object->obj3d->smooth = 1;

                glutPostRedisplay();
            }
            break;

        default:
            /*In the default case we just print the code of the key. This is usefull to define new cases*/
            printf("%d %c\n", key, key);

    }

}

/**
 * @brief Callback function to control the basic keys
 * @param key Key that has been pressed
 * @param x X coordinate of the mouse pointer when the key was pressed
 * @param y Y coordinate of the mouse pointer when the key was pressed
 */
void keyboard(unsigned char key, int x, int y) {

    char* fname = malloc(sizeof (char)*128); /* Note that scanf adds a null character at the end of the vector*/
    int read = 0;
    object *auxiliar_object = 0;
    object* last_changed_object = 0;
    GLdouble* m = 0;
    int i;
    char mota[KG_BUFFER_SIZE];
    GLdouble wd,he,midx,midy;

    switch (key) {
        case 'f':
        case 'F':
            /*Ask for file*/
            printf("%s", KG_MSSG_SELECT_FILE);
            scanf("%s", fname);
            /*Allocate memory for the structure and read the file*/
            auxiliar_object = (object *) malloc(sizeof (object));
            read = read_wavefront(fname, auxiliar_object);
            switch (read) {
                /*Errors in the reading*/
                case 1:
                    printf("%s: %s\n", fname, KG_MSSG_FILENOTFOUND);
                    break;
                case 2:
                    printf("%s: %s\n", fname, KG_MSSG_INVALIDFILE);
                    break;
                case 3:
                    printf("%s: %s\n", fname, KG_MSSG_EMPTYFILE);
                    break;
                    /*Read OK*/
                case 0:
                    /*Insert the new object in the list*/
                    auxiliar_object->next = _first_object;
                    if (auxiliar_object->next == 0)
                        _object_to_transform = auxiliar_object;
                    _first_object = auxiliar_object;
                    _selected_object = _first_object;
                    printf("%s\n",KG_MSSG_FILEREAD);
                    break;
            }
            glutPostRedisplay();
            break;

        case 9: /* <TAB> */
            // Aukeratutako objektua ez bada existitzen, hau da, ez badago aukeratutako objekturik
            if (_selected_object != NULL)
            {
                _selected_object = _selected_object->next;
                /*The selection is circular, thus if we move out of the list we go back to the first element*/
                if (_selected_object == 0) _selected_object = _first_object;

                _object_to_transform = _selected_object;
            }
            glutPostRedisplay();
            break;

        case 127: /* <SUPR> */
            // Aukeratutako objektua ez bada existitzen, hau da, ez badago aukeratutako objekturik
            if (_selected_object == NULL)
                break;
            /*Erasing an object depends on whether it is the first one or not*/
            if (_selected_object == _first_object)
            {
                /*To remove the first object we just set the first as the current's next*/
                _first_object = _first_object->next;
                /*Once updated the pointer to the first object it is save to free the memory*/
                free(_selected_object);
                /*Finally, set the selected to the new first one*/
                _selected_object = _first_object;
            } else {
                /*In this case we need to get the previous element to the one we want to erase*/
                auxiliar_object = _first_object;
                while (auxiliar_object->next != _selected_object)
                    auxiliar_object = auxiliar_object->next;
                /*Now we bypass the element to erase*/
                auxiliar_object->next = _selected_object->next;
                /*free the memory*/
                free(_selected_object);
                /*and update the selection*/
                _selected_object = auxiliar_object;
            }
            glutPostRedisplay();
            break;

        case '-':
            /* Ctrl pultstuta badago, */
            if (glutGetModifiers() == GLUT_ACTIVE_CTRL){
                switch (_camera_type)
                {
                    case KG_ORTHO_CAM:
                        /*Increase the projection plane; compute the new dimensions*/
                        wd=(_ortho_x_max-_ortho_x_min)/KG_STEP_ZOOM;
                        he=(_ortho_y_max-_ortho_y_min)/KG_STEP_ZOOM;
                        /*In order to avoid moving the center of the plane, we get its coordinates*/
                        midx = (_ortho_x_max+_ortho_x_min)/2;
                        midy = (_ortho_y_max+_ortho_y_min)/2;
                        /*The the new limits are set, keeping the center of the plane*/
                        _ortho_x_max = midx + wd/2;
                        _ortho_x_min = midx - wd/2;
                        _ortho_y_max = midy + he/2;
                        _ortho_y_min = midy - he/2;
                        break;
                    case KG_PERSPECTIVE_CAM:
                        if (_perspective_camera_fovy * 1.2 < 180)
                            _perspective_camera_fovy *= 1.2;
                        break;
                    case KG_MOVING_CAM:
                        if (_moving_camera_fovy * 1.2 < 180)
                            _moving_camera_fovy *= 1.2;
                        break;
                }
            }
            else
            {
                if (_selected_object != NULL)
                {
                    if (_selected_light >= 0)
                    {
                        /* Fokuaren angelua txikitu */
                        if (_light_properties[_selected_light]->type == KG_SPOT_LIGHT && _light_properties[_selected_light]->spot_cutoff > 1)
                            if (_light_properties[_selected_light]->spot_cutoff > 10)
                                _light_properties[_selected_light]->spot_cutoff -= 10;
                            else
                                _light_properties[_selected_light]->spot_cutoff /= 2;
                    }
                    else
                    {
                        /* Aukeratutako objektua ardatz gutzietan txikitu */
                        transform(modificationX, 0.5, _selected_object);
                        transform(modificationY, 0.5, _selected_object);
                        transform(modificationZ, 0.5, _selected_object);

                    }
                }

            }
            glutPostRedisplay();
            break;

        case '+':
            if (glutGetModifiers() == GLUT_ACTIVE_CTRL){
                switch (_camera_type)
                {
                    case KG_ORTHO_CAM:
                        /*Decrease the projection plane; compute the new dimensions*/
                        wd=(_ortho_x_max-_ortho_x_min)*KG_STEP_ZOOM;
                        he=(_ortho_y_max-_ortho_y_min)*KG_STEP_ZOOM;
                        /*In order to avoid moving the center of the plane, we get its coordinates*/
                        midx = (_ortho_x_max+_ortho_x_min)/2;
                        midy = (_ortho_y_max+_ortho_y_min)/2;
                        /*The the new limits are set, keeping the center of the plane. */
                        _ortho_x_max = midx + wd/2;
                        _ortho_x_min = midx - wd/2;
                        _ortho_y_max = midy + he/2;
                        _ortho_y_min = midy - he/2;
                        break;
                    case KG_PERSPECTIVE_CAM:
                        _perspective_camera_fovy /= 1.2;
                        break;
                    case KG_MOVING_CAM:
                        _moving_camera_fovy /= 1.2;
                        break;
                }
            }
            else
            {
                if (_selected_object != NULL)
                {
                    if (_selected_light >= 0)
                    {
                        /* Fokuaren angelua handitu */
                        if (_light_properties[_selected_light]->type == KG_SPOT_LIGHT && _light_properties[_selected_light]->spot_cutoff < 90)
                            if (_light_properties[_selected_light]->spot_cutoff >= 10)
                                _light_properties[_selected_light]->spot_cutoff += 10;
                            else
                                _light_properties[_selected_light]->spot_cutoff *= 2;
                    }
                    else
                    {
                        /* Aukeratutako objektua ardatz gutzietan handitu. */
                        transform(modificationX, 2, _selected_object);
                        transform(modificationY, 2, _selected_object);
                        transform(modificationZ, 2, _selected_object);

                    }
                }

            }
            glutPostRedisplay();
            break;

        case '?':
            print_help();
            break;

        case 27: /* <ESC> */
            exit(0);
            break;
        case 'I':
        case 'i':
            /* Uneko objektuari buruzko informazio pantailaratu. */
            printf("%s objektuaren informazioa:\n"
                    "\t Alde kopurua: %d\n"
                    "\t Erpin kopurua: %d\n"
                    "\t Kokapena: %s\n"
                    "\t Transformazio kopurua: %d\n",
                    basename(_selected_object->obj3d->file_name),
                    (int) _selected_object->obj3d->num_faces,
                    (int) _selected_object->obj3d->num_vertices,
                    realpath(_selected_object->obj3d->file_name, NULL),
                    _selected_object->num_transformations);


            break;
        case 'B':
        case 'b':
            /* Biraketa aktibatu. */
            _transformation = KG_ROTATE;
            break;

        case 'm':
        case 'M':
            /* Translazio aktibatu. */
            _transformation = KG_TRANSLATE;
            break;
        case 't':
        case 'T':
            /* Tamaina aldaketa aktibatu. */
            _transformation = KG_MODIFY;
            break;
        case 'r':
        case 'R':
            /* Islapena aktibatu. */
            _transformation = KG_REFLECT;
            break;

        case 'g':
        case 'G':
            if (_object_to_transform->state%2 == 1)
                break;
            /* Transformazio globala aktibatu. */
            _transformation_objective = KG_GLOBAL_TRANSFORMATION;
            break;

        case 'l':
        case 'L':
            /* Transformazio lokala aktibatu. */
            _transformation_objective = KG_LOCAL_TRANSFORMATION;
            break;

        case 'q':
        case 'Q':
            /* AvPag pultsatu balitz bezala egin. */
            page_down(_object_to_transform);
            glutPostRedisplay();
            break;

        case 'w':
        case 'W':
            /* RePag pultsatu balitz bezala egin. */
            page_up(_object_to_transform);
            glutPostRedisplay();
            break;

        case 26: /* Ctrl+Z */
            undo(_object_to_transform);
            glutPostRedisplay();
            break;

        case 25: /* Ctrl-Y */
            redo(_object_to_transform);
            glutPostRedisplay();
            break;

        case 'O':
        case 'o':
            _object_to_transform = _selected_object;
            _selected_light = -1;
            break;
        case 'A':
        case 'a':
            if (_selected_light >= 0)
                _object_to_transform = _light[_selected_light];
            break;

        case 'K':
        case 'k':
            /* Aldaketak uneko kamarari egiteko */
            switch(_camera_type)
            {
                case KG_PERSPECTIVE_CAM:
                    _object_to_transform = _perspective_camera;
                    break;

                case KG_MOVING_CAM:
                    if (_moving_camera->state%2 == 1)
                        _transformation_objective = KG_LOCAL_TRANSFORMATION;
                    _object_to_transform = _moving_camera;
                    break;

            }
            _selected_light = -1;

            break;

        case 'C':
        case 'c':
            /* Kamara aldatu */
            _camera_type = (_camera_type + 1) % 3;
            glutPostRedisplay();
            break;


        case 13:
            if (glutGetModifiers() == GLUT_ACTIVE_CTRL){
                /* Aukeratutako objektuaren azken transformazio matizea bistaratu */
                if (_object_to_transform != 0 && _object_to_transform->transformations_last != 0)
                    showMatrix(_object_to_transform->transformations_last->matrix);
                else
                    printf("No matrix found\n");
            }
            else
            {
                /* Enter */
                /* Argiaztapena gaitu/desgaitu */
                toggleLighting();
                if (_object_to_transform->state == KG_LIGHT)
                {
                    _object_to_transform = _selected_object;
                    _selected_light = -1;
                }
            }

            glutPostRedisplay();
            break;
        case 48: /* 0 */
            if (_selected_light >= 0)
            {
                /* Argi mota aldatu */
                switch(_light_properties[_selected_light]->type)
                {
                    case KG_POINT_LIGHT:
                        _light_properties[_selected_light]->type = KG_DIRECTIONAL_LIGHT;
                        break;
                    case KG_DIRECTIONAL_LIGHT:
                        _light_properties[_selected_light]->type = KG_SPOT_LIGHT;
                        break;
                    case KG_SPOT_LIGHT:
                        _light_properties[_selected_light]->type = KG_POINT_LIGHT;
                        break;
                    default:
                        break;
                }

                glutPostRedisplay();
            }
            break;

        case 's':
        case 'S':
            /* Argiari buruzko informazioa */

            /* Argiztapena */
            if (glIsEnabled(GL_LIGHTING))
                printf("Argiztapena aktibatuta dago\n");
            else
                printf("Argiztapena itzalita dago\n");

            /* Argi bakoitzaren egoera */
            for (i = 0; i < KG_MAX_LIGHTS; i++)
            {
                switch(_light_properties[i]->type)
                {
                    case KG_SPOT_LIGHT:
                        strcpy(mota, KG_NAME_SPOT_LIGHT);
                        break;
                    case KG_POINT_LIGHT:
                        strcpy(mota, KG_NAME_POINT_LIGHT);
                        break;
                    case KG_DIRECTIONAL_LIGHT:
                        strcpy(mota, KG_NAME_DIRECTIONAL_LIGHT);
                        break;
                }

                if (lightIsEnabled(i))
                    printf("%d. argia: PIZTUTA\t\tMota: %s\n", i+1, mota);
                else
                    printf("%d. argia: ITZALITA\t\tMota: %s\n", i+1, mota);

            }

            break;


        case 49: /* 1 */
            if (glIsEnabled(GL_LIGHTING))
                if (glIsEnabled(GL_LIGHT0))
                    _selected_light = 0;
                else
                    printf("%d. argia ez dago piztuta.\n", key - 48);
            else
                printf("Argiztapena itzalita dago.\n");
            break;
        case 50: /* 2 */
            if (glIsEnabled(GL_LIGHTING))
                if (glIsEnabled(GL_LIGHT1))
                    _selected_light = 1;
                else
                    printf("%d. %s\n", key - 48, KG_MSSG_LIGHT_OFF);
            else
                printf("%s\n", KG_MSSG_LIGHTING_OFF);
            break;
        case 51: /* 3 */
            if (glIsEnabled(GL_LIGHTING))
                if (glIsEnabled(GL_LIGHT2))
                    _selected_light = 2;
                else
                    printf("%d. argia ez dago piztuta.\n", key - 48);
            else
                printf("Argiztapena itzalita dago.\n");
            break;
        case 52: /* 4 */
            if (glIsEnabled(GL_LIGHTING))
                if (glIsEnabled(GL_LIGHT3))
                    _selected_light = 3;
                else
                    printf("%d. argia ez dago piztuta.\n", key - 48);
            else
                printf("Argiztapena itzalita dago.\n");
            break;
        case 53: /* 5 */
            if (glIsEnabled(GL_LIGHTING))
                if (glIsEnabled(GL_LIGHT4))
                    _selected_light = 4;
                else
                    printf("%d. argia ez dago piztuta.\n", key - 48);
            else
                printf("Argiztapena itzalita dago.\n");
            break;

        default:
            /*In the default case we just print the code of the key. This is usefull to define new cases*/
            printf("%d %c\n", key, key);
    }
    /*In case we have do any modification affecting the displaying of the object, we redraw them*/
    /* glutPostRedisplay(); */
}

