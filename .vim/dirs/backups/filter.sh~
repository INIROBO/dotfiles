#!/bin/bash

attributes(){

    count=$(grep "@attribute" $1 | wc -l)
    echo $(($count - 1))

}

sailkatu()
{
    # $1: Input arff
    # $2: Output text file

    echo "Sailkatzen: $1"

    if [ ! -f "$2-HoeffdingTree".txt ]; then
        echo "HoeffdingTree..."
        java -Xmx2048M weka.classifiers.trees.HoeffdingTree -L 2 -S 1 -E 1.0E-7 -H 0.05 -M 0.01 -G 200.0 -N 0.0 -t "$1" -x 10 > "$2-HoeffdingTree".txt
        echo "done"
        printf "\n"
    fi

    if [ ! -f "$2-vote-ibkand2".txt ]; then
        echo "Vote-iBK+2..."
        java -Xmx2048M weka.classifiers.meta.Vote -x 10 -t "$1" -S 1 -B "weka.classifiers.lazy.IBk -K 9 -W 0 -I -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\"" -B "weka.classifiers.lazy.IBk -K 5 -W 0 -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\"" -B "weka.classifiers.bayes.NaiveBayes " -R AVG > "$2-vote-ibkand2".txt
        echo "done"
        printf "\n"
    fi


    if [ ! -f "$2-NaiveBayes.txt" ]; then
        echo "Naive Bayes..."
        java -Xmx2048M weka.classifiers.bayes.NaiveBayes -t "$1" -x 10 > "$2-NaiveBayes.txt"
        echo "done"
        printf "\n"
    fi
}

if [ "$1" = "" ]
then
    dir=$(pwd)
else
    if [[ ! -e "$(readlink -f $1)" ]]; then
        echo "$1 ez da existitzen"
        exit 1
    fi
    if [[ ! -d "$(readlink -f $1)" ]]; then
        echo "$1 ez da direktorio bat"
        exit 2
    fi

    dir="${1%/*}"
fi


if [ "$2" = "" ]
then
    classpath=/usr/share/java/weka/weka.jar
else
    if [[ ! -e "$(readlink -f $2)" ]]; then
        echo "$2 ez da existitzen"
        exit 3
    fi
    if [[ ! -f "$(readlink -f $2)" ]]; then
        echo "$2 ez da fitxategi bat"
        exit 4
    fi
    echo 4

    classpath="$2"
fi

export CLASSPATH="$classpath"

wekafolder="$dir/weka/"
filterfolder="$wekafolder""filter/"
mkdir $filterfolder 2> /dev/null

declare -A convert
convert[1]="gaussian-blur"
convert[2]="motion-blur"
convert[3]="sketch"

declare -A arff
arff[1]="$wekafolder""gaussian-blur/""color_layout_filter.arff"
arff[2]="$wekafolder""motion-blur/""color_layout_filter.arff"
arff[3]="$wekafolder""sketch/""color_layout_filter.arff"

for i in `seq 1 3`;
do
    printf "i = $i\n"
    file=${arff[$i]}
    echo $file
    printf "\n"

    echo "BestFirst"
    printf "\n"

    new_arff="$filterfolder""${convert[$i]}-BestFirst.arff"

    if [ ! -f "$new_arff" ]; then
        java -Xmx2048M weka.filters.supervised.attribute.AttributeSelection -i "$file" -o "$new_arff" -E "weka.attributeSelection.CfsSubsetEval -P 1 -E 1" -S "weka.attributeSelection.BestFirst -D 1 -N 5"
    fi

    sailkatu "$new_arff" "$filterfolder""BestFirst-${convert[$i]}"

    echo "Ranker"

    declare -A N
    N[1]=$(attributes $new_arff)
    N[2]=$(expr ${N[1]} / 2)
    N[3]=$(expr ${N[2]} + ${N[1]})

    for j in `seq 1 3`;
    do
        printf "j = $j\n"
        echo "N = ${N[$j]}"

        ranker_arff="$filterfolder""${convert[$i]}-N=${N[$j]}-Ranker"

        echo "$ranker_arff"

        # GainRatioAttributeEval
        if [ ! -f "$ranker_arff-GainRatioAttributeEval.arff" ]; then
            java -Xmx2048M weka.filters.supervised.attribute.AttributeSelection -i "$file" -o "$ranker_arff-GainRatioAttributeEval.arff" -E "weka.attributeSelection.GainRatioAttributeEval " -S "weka.attributeSelection.Ranker -T -1.7976931348623157E308 -N ${N[$j]}"
        fi

        sailkatu "$ranker_arff-GainRatioAttributeEval.arff" "$filterfolder""${convert[$i]}-N=${N[$j]}-GainRatioAttributeEval"


        # OneRAttributeEval
        if [ ! -f "$ranker_arff-OneRAttributeEval.arff" ]; then
            java -Xmx2048M weka.filters.supervised.attribute.AttributeSelection -i "$file" -o "$ranker_arff-OneRAttributeEval.arff" -E "weka.attributeSelection.OneRAttributeEval -S 1 -F 10 -B 6" -S "weka.attributeSelection.Ranker -T -1.7976931348623157E308 -N ${N[$j]}"
        fi

        sailkatu "$ranker_arff-OneRAttributeEval.arff" "$filterfolder""${convert[$i]}-N=${N[$j]}-OneRAttributeEval"


        # SVMAttributeEval
        if [ ! -f "$ranker_arff-SVMAttributeEval.arff" ]; then
            java -Xmx2048M weka.Run weka.filters.supervised.attribute.AttributeSelection -i "$file" -o "$ranker_arff-SVMAttributeEval.arff" -c last -E "weka.attributeSelection.SVMAttributeEval -X 1 -Y 0 -Z 0 -P 1.0E-25 -T 1.0E-10 -C 1.0 -N 0" -S "weka.attributeSelection.Ranker -T -1.7976931348623157E308 -N ${N[$j]}"
        fi

        sailkatu "$ranker_arff-SVMAttributeEval.arff" "$filterfolder""${convert[$i]}-N=${N[$j]}-SVMAttributeEval"

    done


done
