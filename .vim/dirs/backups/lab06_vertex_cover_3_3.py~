from time import time

def getValueNodesAdj(graph, cover, value, objectiveValue = None):
    '''
    Returns the list containing the nodes that are adjacent to
    those with the given value on the given cover and have the value of
    objectiveValue.
    '''
    result = []
    for i in range(len(graph)):
        if cover[i] == value:
            for j in range(len(graph[i])):
                if graph[i][j] and cover[j] == objectiveValue:
                    result.append(j)


    return result

def partial_validity_check(cover, graph):
    '''
    Grafo bat eta cover partzial bat emanda, vertex-cover lortu daitekeen
    adierazten du.
    None balioak bat balira bezala kontsideratzen dira
    '''
    for i in range(len(graph)):
        if cover[i] == 0:
            for j in range(len(graph[i])):
                if cover[j] == 0:
                    if graph[i][j] == 1:
                        return False
    return True

def partial_validity_checkComplete(cover, graph):
    '''
    Grafo bat eta cover partzial bat emanda, vertex-cover dagoen adierazten du.
    None balioak zero balira bezala kontsideratzen dira
    '''
    for i in range(len(graph)):
        if not cover[i]:
            for j in range(i, len(graph[i])):
                if not cover[j]:
                    if graph[i][j]:
                        return False
    return True

# vertex_cover_tree-k bilaketa zuhaitza hasieratu eta recursive_vertex_cover-i deitzen dio
def vertex_cover_tree(graph):
    n = len(graph)
    cover = [None]*n
    return recursive_vertex_cover(graph, cover)


def getOneEdge(graph, include = None):
    '''
    Grafo bat emanda eta nahi bada aztertu nahi diren nodoak emanda,
    Bertako ertz bat itzultzen du.
    '''
    if include == None:
        include = range(len(graph))

    for i in range(len(include)):
        for j in include[i+1:]:
            if graph[include[i]][j]:
                return include[i], j
    return ()



def iterative_vertex_cover(graph):

    length = len(graph)
    # Hemen gordeko dira aztertuko diren cover posibleak
    toCheck = [list([None]*length)]
    results = []

    # Cover batek duen 0 kopuru maximo
    max_cover = 0

    # Aztertu cover posibleak baldin badaude
    while len(toCheck) > 0:
        for i in range(len(toCheck)):
            # Aztertuko den coverra
            peak = toCheck.pop(0)


            # Honela emaitza lortzea matematikoki ezinezkoa bada
            # edo ertz bat ezin izango bada estali ez jarrraitu,
            # bestela, ez dago arazorik
            if partial_validity_check(peak, graph):
                # Soluzio posible bat, gehitu emaitzen bektorera
                if None not in peak:
                    # Beteta dago
                    results.append( peak )

                # Nodo bat aztertzeko gelditzen den kasurako
                elif peak.count(None) == 1:
                    # Azkenekoa izan beharra dauka, beti lehen None balioak
                    # erabiltzen direlako hurrengo esleipenerako
                    last_index = peak.index(None)
                    peak[last_index] = 0
                    # 0 esleitzen bazaio, derrigorrezkoa da nodo honek zituen
                    # ertzak beste nodoren batek estali izana, bestela, 1ekoa
                    # esleitu beharko zaio.
                    for j in range(len(peak)):
                        if graph[last_index][j] == 1 and peak[j]==0:
                            peak[last_index] = 1
                    results.append( peak )
                else:
                    # Lortu aztertu gabeko nodoak
                    CoverTODO = [i for i in range(len(peak))\
                                 if peak[i] == None]
                    # Lortu aztertu gabeko nodoen arteko ertz bat
                    edge = getOneEdge(graph, include = CoverTODO)

                    # Ertzik egon ezean
                    if not len(edge):
                        # Gelditzen direnak 0 batez konektatuta badaude, gehitu
                        a = getValueNodesAdj(graph, peak, 0, None)
                        b = []
                        for i in range(len(peak)):
                            if peak[i] != None:
                                b.append(peak[i])
                            elif i in a:
                                b.append(1)
                            else:
                                b.append(0)


                        results.append(b)

                    else:
                        # Jarraitu balioak esleitzen
                        v = edge[0]
                        u = edge[1]

                        # Kasu bakoitza aztertzeko gorde
                        peak01 = list(peak)
                        peak01[u] = 0
                        peak01[v] = 1
                        toCheck.append(peak01)

                        peak10 = list(peak)
                        peak10[u] = 1
                        peak10[v] = 0
                        toCheck.append(peak10)

                        peak11 = list(peak)
                        peak11[u] = 1
                        peak11[v] = 1
                        toCheck.append(peak11)

    # Itzuli 1 kopuru txikiena duen konbinazioa
    min = float('inf')
    toReturn = []
    for i in range(len(results)):
        count = results[i].count(1)
        if count < min:
            min = count
            toReturn = list(results[i])

    return toReturn


# recursive_vertex_cover-ek bilaketa zuhaitza errekurtsiboki eraikitzen du
def recursive_vertex_cover(graph, cover):

    ############
    # TODO: Funtzioaren zati hau programatu
    #
    # Baliozko cover bat eraikitzea dagoela frogatu
    # Posible ez bada, [1]*len(cover) bueltatu
    # Bestela,
    # cover-a osoa bada, bueltatu
    # cover-a osoa ez bada, cover-ean ez dauden bi u eta v nodo topatu
    # Nodo bakarra badago, cover-aren parte izan behar duen ala ez erabaki
    # eta behin eginda, cover osoa bueltatu
    # Bestela, u eta v-rekin jarraitu
    jarraitu = False
    # Grafoan oraindik vertex-coverra lortu badaiteke
    if partial_validity_check(cover, graph):
        # Ez badago None balio gehiago, itzuli, bestela jarraitu
        if None not in cover:
            return cover
        else:
            jarraitu = True

    else:
        # Itzuli balio handiena
        return [1]*len(cover)

    if jarraitu:
        # None bakarra gelditzen den kasurako
        if cover.count(None) == 1:
            u = cover.index(None)
            copy_cover = list(cover)
            cover[u] = 0
            c0 = recursive_vertex_cover(graph, cover)
            cover = list(copy_cover)
            cover[u] = 1
            c1 = recursive_vertex_cover(graph, cover)
            return c0 if c0.count(1) < c1.count(1) else c1

        # Hemen gorde aztertu ez diren nodoak
        CoverTODO = [i for i in range(len(cover)) if cover[i] == None]
        # Lortu ertz bat grafoan aztertu gabeko nodoen artean
        edge = getOneEdge(graph, include = CoverTODO)

        # Ertzik egon ezean
        if not len(edge):
            # Gelditzen direnak 0 batezkonektatuta badaude, gehitu
            a = getValueNodesAdj(graph, cover, 0, None)
            b = []
            for i in range(len(cover)):
                if cover[i] != None:
                    b.append(cover[i])
                elif i in a:
                    b.append(1)
                else:
                    b.append(0)


            return b
            # Aztertu ea bukatutzan eman dezakegun konbinazioa
            # if partial_validity_checkComplete(cover, graph):
            #     # Bihurtu None-ak 0 etan
            #     return [i if i != None else 0 for i in cover]
            # else:
            #     # Balio posiblerik handiena
            #     return [1]*len(cover)
        else:
            # Jarraitu balioak ematen
            v = edge[0]
            u = edge[1]

    # Zure kodearen amaiera
    # Hurrengoak bilaketa zuhaitzaren bi adarrak irekitzen ditu
    # Ez ezer ikutu
    ##############

        copy_cover = list(cover)
        cover[u] = 1
        cover[v] = 0
        c10 = recursive_vertex_cover(graph, cover)
        cover = list(copy_cover)
        cover[u] = 0
        cover[v] = 1
        c01 = recursive_vertex_cover(graph, cover)
        cover = list(copy_cover)
        cover[u] = 1
        cover[v] = 1
        c11 = recursive_vertex_cover(graph, cover)
        # print(c10, c01, c11)
        if c10.count(1) <= min(c01.count(1), c11.count(1)):
            return c10
        elif c01.count(1) <= c11.count(1):
            return c01
        else:
            return c11

def testRec():

    g1 =  [[0, 1],
           [1, 0]]

    g2 = [[0, 1, 1],
          [1, 0, 0],
          [1, 0, 0]]


    g3 = [[0, 1, 1, 1, 1],
          [1, 0, 0, 0, 1],
          [1, 0, 0, 1, 1],
          [1, 0, 1, 0, 1],
          [1, 1, 1, 1, 0]]


    g4 = [[0, 1, 1, 1],
          [1, 0, 1, 0],
          [1, 1, 0, 1],
          [1, 0, 1, 0]]


    g5 = [[0, 1, 1, 0, 0, 0],
          [1, 0, 0, 1, 1, 0],
          [1, 0, 0, 1, 1, 1],
          [0, 1, 1, 0, 0, 1],
          [0, 1, 1, 0, 0, 0],
          [0, 0, 1, 1, 0, 0]]

    g6 = [[0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0],
          [1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
          [1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0],
          [0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
          [0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1],
          [0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0],
          [1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0],
          [1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0],
          [1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0],
          [1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],
          [1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1],
          [1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1],
          [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1],
          [0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1],
          [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0]]





##    recursive_vertex_cover funtzioa probatzeko iruzkinak kendu
    assert vertex_cover_tree(g1) in [[1,0],[0,1]]
    assert vertex_cover_tree(g2)  == [1,0,0]
    assert vertex_cover_tree(g3) in [[1, 0, 1, 0, 1],
                                     [1, 0, 0, 1, 1]]
    assert vertex_cover_tree(g4)  == [1, 0, 1, 0]
    assert vertex_cover_tree(g5)  in  [[0, 1, 1, 1, 0, 0], [0, 1, 1, 0, 0, 1]]


    assert vertex_cover_tree(g6) in [[1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
                                    [1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1],
                                    [1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1],
                                    [1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1],
                                    [1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
                                    [1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1],
                                    [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1],
                                    [1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0],
                                    [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1],
                                    [1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1],
                                    [1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1]]

def testIte():

    g1 =  [[0, 1],
           [1, 0]]

    g2 = [[0, 1, 1],
          [1, 0, 0],
          [1, 0, 0]]


    g3 = [[0, 1, 1, 1, 1],
          [1, 0, 0, 0, 1],
          [1, 0, 0, 1, 1],
          [1, 0, 1, 0, 1],
          [1, 1, 1, 1, 0]]


    g4 = [[0, 1, 1, 1],
          [1, 0, 1, 0],
          [1, 1, 0, 1],
          [1, 0, 1, 0]]


    g5 = [[0, 1, 1, 0, 0, 0],
          [1, 0, 0, 1, 1, 0],
          [1, 0, 0, 1, 1, 1],
          [0, 1, 1, 0, 0, 1],
          [0, 1, 1, 0, 0, 0],
          [0, 0, 1, 1, 0, 0]]

    g6 = [[0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0],
          [1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
          [1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0],
          [0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
          [0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1],
          [0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0],
          [1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0],
          [1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0],
          [1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0],
          [1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],
          [1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1],
          [1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1],
          [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1],
          [0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1],
          [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0]]

    assert iterative_vertex_cover(g1) in [[1,0],[0,1]]
    assert iterative_vertex_cover(g2)  == [1,0,0]
    assert iterative_vertex_cover(g3) in [[1, 0, 1, 0, 1],
                                     [1, 0, 0, 1, 1]]
    assert iterative_vertex_cover(g4)  == [1, 0, 1, 0]
    assert iterative_vertex_cover(g5)  in  [[0, 1, 1, 1, 0, 0], [0, 1, 1, 0, 0, 1]]


    assert iterative_vertex_cover(g6) in [[1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
                                    [1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1],
                                    [1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
                                    [1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0],
                                    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1],
                                    [1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1],
                                    [1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
                                    [1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1],
                                    [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0],
                                    [1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1],
                                    [1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0],
                                    [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1],
                                    [1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1],
                                    [1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1],
                                    [1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1]]

start_time = time()
testRec()
elapsed_time = time() - start_time
print("Elapsed time - Recursive : %0.10f seconds." % elapsed_time)

start_time = time()
testIte()
elapsed_time = time() - start_time
print("Elapsed time - Iterative: %0.10f seconds." % elapsed_time)
