" LaTeX                                                                    
map <buffer> <C-m> :!zathura %:p:r.pdf & disown <ENTER><ENTER>
map <buffer> <C-l> :w <bar> :!pdflatex -output-directory %:p:h %:p<ENTER>

" imap <C-CR> <CR>\\item 
